part of 'page_bloc.dart';

abstract class PageEvent extends Equatable {
  const PageEvent();
}

class GoToSplashPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToMainPage extends PageEvent {
  final int bottomNavBarIndex;

  GoToMainPage({this.bottomNavBarIndex = 0});

  @override
  List<Object> get props => [bottomNavBarIndex];
}

class GoToNotificationPage extends PageEvent {
  final PageEvent pageEvent;

  GoToNotificationPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToSchoolPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToLoanPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToElearningPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToConsultationPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToShoppingPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToPromoPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToPromoDetailPage extends PageEvent {
  final Promo promo;
  final PageEvent pageEvent;

  GoToPromoDetailPage(this.promo, this.pageEvent);

  @override
  List<Object> get props => [promo, pageEvent];
}

class GoToNewsDetailPage extends PageEvent {
  final News news;
  final PageEvent pageEvent;

  GoToNewsDetailPage(this.news, this.pageEvent);

  @override
  List<Object> get props => [news, pageEvent];
}

class GoToEventDetailPage extends PageEvent {
  final Event event;
  final PageEvent pageEvent;

  GoToEventDetailPage(this.event, this.pageEvent);

  @override
  List<Object> get props => [event, pageEvent];
}

class GoToChatPage extends PageEvent {
  final Inbox inbox;
  final PageEvent pageEvent;

  GoToChatPage(this.inbox, this.pageEvent);

  @override
  List<Object> get props => [inbox, pageEvent];
}

class GoToSeeallSchoolPage extends PageEvent {
  final String title;
  final SchoolType type;
  final List<School> schools;
  final PageEvent pageEvent;

  GoToSeeallSchoolPage(this.title, this.type, this.schools, this.pageEvent);

  @override
  List<Object> get props => [title, type, schools, pageEvent];
}

class GoToSchoolDetailPage extends PageEvent {
  final School school;
  final PageEvent pageEvent;

  GoToSchoolDetailPage(this.school, this.pageEvent);

  @override
  List<Object> get props => [school, pageEvent];
}

class GoToSchoolInformationPage extends PageEvent {
  final School school;
  final PageEvent pageEvent;

  GoToSchoolInformationPage(this.school, this.pageEvent);

  @override
  List<Object> get props => [school, pageEvent];
}

class GoToSchoolRegistrationPage extends PageEvent {
  final School school;
  final PageEvent pageEvent;

  GoToSchoolRegistrationPage(this.school, this.pageEvent);

  @override
  List<Object> get props => [school, pageEvent];
}

class GoToAccountDetailPage extends PageEvent {
  final User user;
  final PageEvent pageEvent;

  GoToAccountDetailPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class GoToChildrenPage extends PageEvent {
  final PageEvent pageEvent;

  GoToChildrenPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToElearningMaterialPage extends PageEvent {
  final Elearning elearning;
  final PageEvent pageEvent;

  GoToElearningMaterialPage(this.elearning, this.pageEvent);

  @override
  List<Object> get props => [elearning, pageEvent];
}

class GoToElearningDetailPage extends PageEvent {
  final PageEvent pageEvent;

  GoToElearningDetailPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToShoppingDetailPage extends PageEvent {
  final Product product;
  final PageEvent pageEvent;

  GoToShoppingDetailPage(this.product, this.pageEvent);

  @override
  List<Object> get props => [product, pageEvent];
}

class GoToLoanaPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanaPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanbPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanbPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoancPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoancPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoandPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoandPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanePage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanfPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanfPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoangPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoangPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanhPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanhPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoaniPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoaniPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanjPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanjPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoankPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoankPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanlPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanlPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToLoanResultPage extends PageEvent {
  final PageEvent pageEvent;

  GoToLoanResultPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToChangePasswordPage extends PageEvent {
  final User user;
  final PageEvent pageEvent;

  GoToChangePasswordPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class GoToWishlistPage extends PageEvent {
  final PageEvent pageEvent;

  GoToWishlistPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToTransactionPage extends PageEvent {
  final PageEvent pageEvent;

  GoToTransactionPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToTransactionHistoryPage extends PageEvent {
  final PageEvent pageEvent;

  GoToTransactionHistoryPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}
