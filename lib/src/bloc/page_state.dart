part of 'page_bloc.dart';

abstract class PageState extends Equatable {
  const PageState();
}

class OnInitialPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnSplashPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnMainPage extends PageState {
  final int bottomNavBarIndex;

  OnMainPage({this.bottomNavBarIndex = 0});

  @override
  List<Object> get props => [bottomNavBarIndex];
}

class OnNotificationPage extends PageState {
  final PageEvent pageEvent;

  OnNotificationPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnSchoolPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnLoanPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnElearningPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnConsultationPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnShoppingPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnPromoPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnPromoDetailPage extends PageState {
  final Promo promo;
  final PageEvent pageEvent;

  OnPromoDetailPage(this.promo, this.pageEvent);

  @override
  List<Object> get props => [promo, pageEvent];
}

class OnNewsDetailPage extends PageState {
  final News news;
  final PageEvent pageEvent;

  OnNewsDetailPage(this.news, this.pageEvent);

  @override
  List<Object> get props => [news, pageEvent];
}

class OnEventDetailPage extends PageState {
  final Event event;
  final PageEvent pageEvent;

  OnEventDetailPage(this.event, this.pageEvent);

  @override
  List<Object> get props => [event, pageEvent];
}

class OnChatPage extends PageState {
  final Inbox inbox;
  final PageEvent pageEvent;

  OnChatPage(this.inbox, this.pageEvent);

  @override
  List<Object> get props => [inbox, pageEvent];
}

class OnSeeallSchoolPage extends PageState {
  final String title;
  final SchoolType type;
  final List<School> schools;
  final PageEvent pageEvent;

  OnSeeallSchoolPage(this.title, this.type, this.schools, this.pageEvent);

  @override
  List<Object> get props => [title, type, schools, pageEvent];
}

class OnSchoolDetailPage extends PageState {
  final School school;
  final PageEvent pageEvent;

  OnSchoolDetailPage(this.school, this.pageEvent);

  @override
  List<Object> get props => [school, pageEvent];
}

class OnSchoolInformationPage extends PageState {
  final School school;
  final PageEvent pageEvent;

  OnSchoolInformationPage(this.school, this.pageEvent);

  @override
  List<Object> get props => [school, pageEvent];
}

class OnSchoolRegistrationPage extends PageState {
  final School school;
  final PageEvent pageEvent;

  OnSchoolRegistrationPage(this.school, this.pageEvent);

  @override
  List<Object> get props => [school, pageEvent];
}

class OnAccountDetailPage extends PageState {
  final User user;
  final PageEvent pageEvent;

  OnAccountDetailPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class OnChildrenPage extends PageState {
  final PageEvent pageEvent;

  OnChildrenPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnElearningMaterialPage extends PageState {
  final Elearning elearning;
  final PageEvent pageEvent;

  OnElearningMaterialPage(this.elearning, this.pageEvent);

  @override
  List<Object> get props => [elearning, pageEvent];
}

class OnElearningDetailPage extends PageState {
  final PageEvent pageEvent;

  OnElearningDetailPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnShoppingDetailPage extends PageState {
  final Product product;
  final PageEvent pageEvent;

  OnShoppingDetailPage(this.product, this.pageEvent);

  @override
  List<Object> get props => [product, pageEvent];
}

class OnLoanaPage extends PageState {
  final PageEvent pageEvent;

  OnLoanaPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanbPage extends PageState {
  final PageEvent pageEvent;

  OnLoanbPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoancPage extends PageState {
  final PageEvent pageEvent;

  OnLoancPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoandPage extends PageState {
  final PageEvent pageEvent;

  OnLoandPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanePage extends PageState {
  final PageEvent pageEvent;

  OnLoanePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanfPage extends PageState {
  final PageEvent pageEvent;

  OnLoanfPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoangPage extends PageState {
  final PageEvent pageEvent;

  OnLoangPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanhPage extends PageState {
  final PageEvent pageEvent;

  OnLoanhPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoaniPage extends PageState {
  final PageEvent pageEvent;

  OnLoaniPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanjPage extends PageState {
  final PageEvent pageEvent;

  OnLoanjPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoankPage extends PageState {
  final PageEvent pageEvent;

  OnLoankPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanlPage extends PageState {
  final PageEvent pageEvent;

  OnLoanlPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnLoanResultPage extends PageState {
  final PageEvent pageEvent;

  OnLoanResultPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnChangePasswordPage extends PageState {
  final User user;
  final PageEvent pageEvent;

  OnChangePasswordPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class OnWishlistPage extends PageState {
  final PageEvent pageEvent;

  OnWishlistPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnTransactionPage extends PageState {
  final PageEvent pageEvent;

  OnTransactionPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnTransactionHistoryPage extends PageState {
  final PageEvent pageEvent;

  OnTransactionHistoryPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}
