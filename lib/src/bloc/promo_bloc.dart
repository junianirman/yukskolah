import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:yukskolah/src/models/models.dart';
import 'package:yukskolah/src/services/services.dart';

part 'promo_event.dart';
part 'promo_state.dart';

class PromoBloc extends Bloc<PromoEvent, PromoState> {
  PromoBloc() : super(OnInitialPromo());

  @override
  Stream<PromoState> mapEventToState(
    PromoEvent event,
  ) async* {
    if (event is FetchPromo) {
      List<Promo> promo = await PromoServices.getPromo();
      yield PromoLoaded(promo: promo);
    }
  }
}
