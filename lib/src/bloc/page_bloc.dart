import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:yukskolah/src/models/models.dart';

part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  PageBloc() : super(OnInitialPage());

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GoToSplashPage) {
      yield OnSplashPage();
    } else if (event is GoToMainPage) {
      yield OnMainPage(bottomNavBarIndex: event.bottomNavBarIndex);
    } else if (event is GoToNotificationPage) {
      yield OnNotificationPage(event.pageEvent);
    } else if (event is GoToSchoolPage) {
      yield OnSchoolPage();
    } else if (event is GoToLoanPage) {
      yield OnLoanPage();
    } else if (event is GoToElearningPage) {
      yield OnElearningPage();
    } else if (event is GoToConsultationPage) {
      yield OnConsultationPage();
    } else if (event is GoToShoppingPage) {
      yield OnShoppingPage();
    } else if (event is GoToPromoPage) {
      yield OnPromoPage();
    } else if (event is GoToPromoDetailPage) {
      yield OnPromoDetailPage(event.promo, event.pageEvent);
    } else if (event is GoToNewsDetailPage) {
      yield OnNewsDetailPage(event.news, event.pageEvent);
    } else if (event is GoToEventDetailPage) {
      yield OnEventDetailPage(event.event, event.pageEvent);
    } else if (event is GoToChatPage) {
      yield OnChatPage(event.inbox, event.pageEvent);
    } else if (event is GoToSeeallSchoolPage) {
      yield OnSeeallSchoolPage(
          event.title, event.type, event.schools, event.pageEvent);
    } else if (event is GoToSchoolDetailPage) {
      yield OnSchoolDetailPage(event.school, event.pageEvent);
    } else if (event is GoToSchoolInformationPage) {
      yield OnSchoolInformationPage(event.school, event.pageEvent);
    } else if (event is GoToSchoolRegistrationPage) {
      yield OnSchoolRegistrationPage(event.school, event.pageEvent);
    } else if (event is GoToAccountDetailPage) {
      yield OnAccountDetailPage(event.user, event.pageEvent);
    } else if (event is GoToChildrenPage) {
      yield OnChildrenPage(event.pageEvent);
    } else if (event is GoToElearningMaterialPage) {
      yield OnElearningMaterialPage(event.elearning, event.pageEvent);
    } else if (event is GoToElearningDetailPage) {
      yield OnElearningDetailPage(event.pageEvent);
    } else if (event is GoToShoppingDetailPage) {
      yield OnShoppingDetailPage(event.product, event.pageEvent);
    } else if (event is GoToLoanaPage) {
      yield OnLoanaPage(event.pageEvent);
    } else if (event is GoToLoanbPage) {
      yield OnLoanbPage(event.pageEvent);
    } else if (event is GoToLoancPage) {
      yield OnLoancPage(event.pageEvent);
    } else if (event is GoToLoandPage) {
      yield OnLoandPage(event.pageEvent);
    } else if (event is GoToLoanePage) {
      yield OnLoanePage(event.pageEvent);
    } else if (event is GoToLoanfPage) {
      yield OnLoanfPage(event.pageEvent);
    } else if (event is GoToLoangPage) {
      yield OnLoangPage(event.pageEvent);
    } else if (event is GoToLoanhPage) {
      yield OnLoanhPage(event.pageEvent);
    } else if (event is GoToLoaniPage) {
      yield OnLoaniPage(event.pageEvent);
    } else if (event is GoToLoanjPage) {
      yield OnLoanjPage(event.pageEvent);
    } else if (event is GoToLoankPage) {
      yield OnLoankPage(event.pageEvent);
    } else if (event is GoToLoanlPage) {
      yield OnLoanlPage(event.pageEvent);
    } else if (event is GoToLoanResultPage) {
      yield OnLoanResultPage(event.pageEvent);
    } else if (event is GoToChangePasswordPage) {
      yield OnChangePasswordPage(event.user, event.pageEvent);
    } else if (event is GoToWishlistPage) {
      yield OnWishlistPage(event.pageEvent);
    } else if (event is GoToTransactionPage) {
      yield OnTransactionPage(event.pageEvent);
    } else if (event is GoToTransactionHistoryPage) {
      yield OnTransactionHistoryPage(event.pageEvent);
    }
  }
}
