part of 'promo_bloc.dart';

abstract class PromoState extends Equatable {
  const PromoState();

  @override
  List<Object> get props => [];
}

class OnInitialPromo extends PromoState {}

class PromoLoaded extends PromoState {
  final List<Promo> promo;

  PromoLoaded({this.promo});

  @override
  List<Object> get props => [promo];
}
