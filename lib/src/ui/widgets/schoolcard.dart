part of 'widgets.dart';

class SchoolCard extends StatelessWidget {
  final School school;
  final Function onTap;

  const SchoolCard(this.school, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width / 2 - 32,
        height: 210,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(spreadRadius: 1, blurRadius: 6, color: Colors.black38)
            ]),
        child: Column(
          children: [
            Container(
              height: 140,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8)),
                image: DecorationImage(
                    image: AssetImage(school.picturePath), fit: BoxFit.cover),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(12, 12, 12, 6),
              width: 200,
              child: Text(
                school.name,
                style: blackTextFont,
                maxLines: 1,
                overflow: TextOverflow.clip,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 12),
              child: RatingStars(school.rate),
            )
          ],
        ),
      ),
    );
  }
}
