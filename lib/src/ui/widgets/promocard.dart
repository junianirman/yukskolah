part of 'widgets.dart';

class PromoCard extends StatelessWidget {
  final Promo promo;
  final Function onTap;

  const PromoCard(this.promo, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        height: 150,
        width: (MediaQuery.of(context).size.width - 32),
        margin:
            EdgeInsets.fromLTRB(defaultMargin, defaultMargin, defaultMargin, 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          image: DecorationImage(
              image: AssetImage(promo.picturePath), fit: BoxFit.cover),
        ),
      ),
    );
  }
}
