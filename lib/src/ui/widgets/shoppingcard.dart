part of 'widgets.dart';

class ShoppingCard extends StatelessWidget {
  final Product product;
  final Function onTap;

  const ShoppingCard(this.product, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        // width: 200,
        width: MediaQuery.of(context).size.width / 2 - 32,
        height: 210,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(spreadRadius: 1, blurRadius: 6, color: Colors.black38)
            ]),
        child: Column(
          children: [
            Container(
              height: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12)),
                image: DecorationImage(
                    image: AssetImage(product.picturePath),
                    fit: BoxFit.contain),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8, left: 8),
              width: 200,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product.name,
                    style: blackTextFont,
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                  Text(
                      NumberFormat.currency(
                              locale: "id_ID", decimalDigits: 0, symbol: "Rp ")
                          .format(product.price),
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.w400)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 8),
              child: RatingStars(product.seller.rate),
            ),
            Container(
              margin: EdgeInsets.only(left: 8),
              child: Row(
                children: [
                  Text(
                    product.seller.name,
                    style: blackTextFont.copyWith(fontSize: 12),
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
