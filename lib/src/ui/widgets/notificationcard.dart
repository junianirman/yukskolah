part of 'widgets.dart';

class NotificationCard extends StatelessWidget {
  final Notifications notifications;

  const NotificationCard(this.notifications, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      width: (MediaQuery.of(context).size.width - 32),
      margin: EdgeInsets.fromLTRB(
        defaultMargin,
        defaultMargin,
        defaultMargin,
        (dummyNotification.indexOf(notifications) ==
                dummyNotification.length - 1
            ? defaultMargin
            : 0),
      ),
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: greyColor.withOpacity(0.5),
      ),
      child: Row(
        children: [
          Container(
            height: 100,
            width: 100,
            margin: EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                  image: AssetImage(notifications.picturePath),
                  fit: BoxFit.cover),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 2.4 - 16,
            margin: EdgeInsets.all(8),
            child: Text(
              notifications.status,
              style: blackTextFont,
              maxLines: 2,
              overflow: TextOverflow.clip,
            ),
          ),
          Column(
            children: [
              Container(
                margin: EdgeInsets.all(8),
                child:
                    Text(notifications.notificationDate, style: blackTextFont),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
