part of 'widgets.dart';

class ElearningMaterialCard extends StatelessWidget {
  final ElearningMaterial elearningMaterial;
  final Function onTap;

  const ElearningMaterialCard(this.elearningMaterial, {this.onTap, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        height: 100,
        width: (MediaQuery.of(context).size.width - 32),
        margin:
            EdgeInsets.fromLTRB(defaultMargin, defaultMargin, defaultMargin, 0),
        padding: EdgeInsets.all(4),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: greyColor.withOpacity(0.5)),
        child: Row(
          children: [
            Container(
              height: 80,
              width: 100,
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(elearningMaterial.picturePath),
                    fit: BoxFit.cover),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: Text(
                  elearningMaterial.title,
                  style: blackTextFont.copyWith(fontSize: 16),
                  textAlign: TextAlign.justify,
                  maxLines: 8,
                  overflow: TextOverflow.clip,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
