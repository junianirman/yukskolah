part of 'widgets.dart';

class TransactionCard extends StatelessWidget {
  final Transaction transaction;

  const TransactionCard(this.transaction, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          bottom: (dummyTransaction.indexOf(transaction) ==
                  dummyTransaction.length - 1
              ? 0
              : defaultMargin)),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: mainColor),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 24,
                    width: 24,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(transaction.transactionType ==
                                TransactionType.school
                            ? 'assets/ic_list.png'
                            : transaction.transactionType ==
                                        TransactionType.loan ||
                                    transaction.transactionType ==
                                        TransactionType.elearning
                                ? 'assets/ic_loan.jpg'
                                : transaction.transactionType ==
                                        TransactionType.product
                                    ? 'assets/ic_bag.jpg'
                                    : transaction.transactionType ==
                                                TransactionType.consultation ||
                                            transaction.transactionType ==
                                                TransactionType.psycological
                                        ? 'assets/ic_consultation.png'
                                        : 'assets/user_pic.jpg'),
                      ),
                    ),
                  ),
                  SizedBox(width: 4),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          transaction.transactionType == TransactionType.school
                              ? "Sekolah"
                              : transaction.transactionType ==
                                      TransactionType.loan
                                  ? "Loan"
                                  : transaction.transactionType ==
                                          TransactionType.product
                                      ? "Belanja"
                                      : transaction.transactionType ==
                                              TransactionType.elearning
                                          ? "E-Learning"
                                          : transaction.transactionType ==
                                                  TransactionType.consultation
                                              ? "Consultation"
                                              : transaction.transactionType ==
                                                      TransactionType
                                                          .psycological
                                                  ? "Psycological Test"
                                                  : "",
                          style: blackTextFont),
                      Text(transaction.orderDate, style: blackTextFont),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Color(0xFFF1FC74),
                    ),
                    child: Text(
                      transaction.status.capitalizeFirst,
                      style: blackTextFont.copyWith(fontSize: 14),
                    ),
                  ),
                  SizedBox(width: 4),
                  Icon(
                    Icons.more_vert_rounded,
                    size: 14,
                  ),
                ],
              ),
            ],
          ),
          Divider(thickness: 1, color: mainColor),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                    image: AssetImage(transaction.transactionType ==
                            TransactionType.school
                        ? transaction.school.picturePath
                        : transaction.transactionType == TransactionType.loan
                            ? transaction.loan.picturePath
                            : transaction.transactionType ==
                                    TransactionType.product
                                ? transaction.product.picturePath
                                : transaction.transactionType ==
                                        TransactionType.elearning
                                    ? transaction.elearning.picturePath
                                    : transaction.transactionType ==
                                            TransactionType.consultation
                                        ? transaction.consultant.picturePath
                                        : transaction.transactionType ==
                                                TransactionType.psycological
                                            ? transaction
                                                .psycological.picturePath
                                            : "assets/user_pic.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(width: 8),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 1.6,
                    child: Text(
                        transaction.transactionType == TransactionType.school
                            ? transaction.school.name
                            : transaction.transactionType ==
                                    TransactionType.loan
                                ? transaction.loan.name
                                : transaction.transactionType ==
                                        TransactionType.product
                                    ? transaction.product.name
                                    : transaction.transactionType ==
                                            TransactionType.elearning
                                        ? transaction.elearning.lecturer
                                        : transaction.transactionType ==
                                                TransactionType.consultation
                                            ? transaction.consultant.name
                                            : transaction.transactionType ==
                                                    TransactionType.psycological
                                                ? transaction.psycological.name
                                                : "",
                        style: blackTextFont,
                        overflow: TextOverflow.clip,
                        maxLines: 1),
                  ),
                  Text(
                      transaction.transactionType == TransactionType.school
                          ? "${transaction.quantity} Pendaftaran"
                          : transaction.transactionType == TransactionType.loan
                              ? "${transaction.quantity} Pengajuan"
                              : transaction.transactionType ==
                                      TransactionType.product
                                  ? "${transaction.quantity} Barang"
                                  : transaction.transactionType ==
                                          TransactionType.elearning
                                      ? "${transaction.quantity} Modul Elearning"
                                      : transaction.transactionType ==
                                                  TransactionType
                                                      .consultation ||
                                              transaction.transactionType ==
                                                  TransactionType.psycological
                                          ? "${transaction.quantity} Consultation"
                                          : "${transaction.quantity} Item",
                      style: blackTextFont),
                ],
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      transaction.transactionType == TransactionType.school
                          ? "Biaya Pendaftaran"
                          : transaction.transactionType == TransactionType.loan
                              ? "Total Pengajuan"
                              : transaction.transactionType ==
                                      TransactionType.product
                                  ? "Total Belanja"
                                  : "Total Pembelian",
                      style: blackTextFont),
                  Text(
                    NumberFormat.currency(
                            locale: "id_ID", decimalDigits: 0, symbol: "Rp ")
                        .format(transaction.total),
                    style: blackTextFont.copyWith(fontWeight: FontWeight.w400),
                  ),
                ],
              ),
              transaction.transactionType == TransactionType.loan
                  ? Container(
                      padding: EdgeInsets.all(6),
                      decoration: BoxDecoration(
                        color: orangeColor,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Text(
                        "Ajukan Lagi",
                        style: blackTextFont.copyWith(fontSize: 14),
                      ),
                    )
                  : transaction.transactionType == TransactionType.product
                      ? Container(
                          padding: EdgeInsets.all(6),
                          decoration: BoxDecoration(
                            color: orangeColor,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: Text(
                            "Beli Lagi",
                            style: blackTextFont.copyWith(fontSize: 14),
                          ),
                        )
                      : Container(),
            ],
          ),
        ],
      ),
    );
  }
}
