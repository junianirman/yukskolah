part of 'widgets.dart';

class ElearningCard extends StatelessWidget {
  final Elearning elearning;
  final Function onTap;

  const ElearningCard(this.elearning, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        // width: 200,
        width: MediaQuery.of(context).size.width / 2 - 32,
        height: 210,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(spreadRadius: 1, blurRadius: 6, color: Colors.black38)
            ]),
        child: Column(
          children: [
            Container(
              height: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12)),
                image: DecorationImage(
                    image: AssetImage(elearning.picturePath),
                    fit: BoxFit.cover),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8, left: 8),
              width: 200,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    elearning.major,
                    style: blackTextFont,
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                  Text(
                    "Kelas : " + "${elearning.kelas}",
                    style: blackTextFont.copyWith(fontSize: 12),
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                  Text(
                    "Kurikulim : " + elearning.curriculum,
                    style: blackTextFont.copyWith(fontSize: 12),
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 8),
              child: RatingStars(elearning.rate),
            ),
            Container(
              margin: EdgeInsets.only(left: 8),
              child: Row(
                children: [
                  Text(
                    elearning.lecturer,
                    style: blackTextFont.copyWith(fontSize: 12),
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
