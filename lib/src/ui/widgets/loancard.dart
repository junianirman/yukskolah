part of 'widgets.dart';

class LoanCard extends StatelessWidget {
  final Loan loan;

  const LoanCard(this.loan, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      margin:
          EdgeInsets.fromLTRB(defaultMargin, defaultMargin, defaultMargin, 0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: mainColor)),
      child: Container(
        margin: EdgeInsets.all(8),
        child: Column(
          children: [
            Row(
              children: [
                Icon(Icons.list_alt_rounded, size: 40),
                SizedBox(width: 4),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(loan.name, style: blackTextFont),
                      Text(loan.title,
                          style: blackTextFont.copyWith(fontSize: 12))
                    ],
                  ),
                ),
              ],
            ),
            Divider(thickness: 1, color: mainColor),
            Expanded(
              child: Row(
                children: [
                  Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                          image: AssetImage(loan.picturePath),
                          fit: BoxFit.cover),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Suku Bunga perbulan",
                              style: blackTextFont,
                            ),
                            Text("${loan.bunga}%")
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Cicilan Perbulan",
                              style: blackTextFont,
                            ),
                            Text(
                                NumberFormat.currency(
                                        locale: "id_ID",
                                        decimalDigits: 0,
                                        symbol: "Rp ")
                                    .format(loan.cicilan),
                                style: blackTextFont.copyWith(
                                    fontWeight: FontWeight.w400)),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              child: Text(
                                "Lihat Detail",
                                style: linkTextFont,
                              ),
                              onTap: () {},
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2 - 24,
                              margin: EdgeInsets.only(top: 8),
                              height: 40,
                              padding: EdgeInsets.only(left: 32),
                              child: RaisedButton(
                                onPressed: () {
                                  // context
                                  //     .read<PageBloc>()
                                  //     .add(GoToLoanfPage(widget.pageEvent));
                                },
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                color: orangeColor,
                                child: Text(
                                  'Ajukan Sekarang',
                                  style: blackTextFont,
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
