part of 'widgets.dart';

class ConsultationCard extends StatelessWidget {
  final Consultant consultant;

  const ConsultationCard(this.consultant, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 265,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.fromLTRB(
        defaultMargin,
        defaultMargin,
        defaultMargin,
        (dummyConsultant.indexOf(consultant) == dummyConsultant.length - 1
            ? defaultMargin
            : 0),
      ),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
          color: darkGreyColor, borderRadius: BorderRadius.circular(8)),
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 150,
                width: MediaQuery.of(context).size.width / 3.2,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                      image: AssetImage(consultant.picturePath),
                      fit: BoxFit.cover),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 3.2,
                margin: EdgeInsets.only(top: 8),
                height: 40,
                child: RaisedButton(
                  onPressed: () {},
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  color: Color(0xFFFFFFFF),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        'Date',
                        style: blackTextFont,
                      ),
                      Icon(Icons.date_range),
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 3.2,
                margin: EdgeInsets.only(top: 8),
                height: 40,
                child: RaisedButton(
                  onPressed: () {},
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  color: Color(0xFFFFFFFF),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        'Time',
                        style: blackTextFont,
                      ),
                      Icon(Icons.access_time_rounded),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: 8),
          Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(consultant.name, style: blackTextFont),
                  Text(consultant.profession, style: blackTextFont),
                  Text("${consultant.age} Tahun", style: blackTextFont),
                  RatingStars(consultant.rate),
                  Container(
                    width: MediaQuery.of(context).size.width / 2 + 24,
                    margin: EdgeInsets.only(bottom: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Price ", style: blackTextFont),
                        Text(
                            NumberFormat.currency(
                                    locale: "id_ID",
                                    decimalDigits: 0,
                                    symbol: "Rp ")
                                .format(consultant.price),
                            style: blackTextFont.copyWith(
                                fontWeight: FontWeight.w400)),
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Jadwal Konsultasi", style: blackTextFont),
                  Column(
                    children: consultant.setupDateTime
                        .map((e) => Container(
                              width: MediaQuery.of(context).size.width / 2 + 24,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "${e.day.dayName}",
                                    style: blackTextFont,
                                    overflow: TextOverflow.clip,
                                    maxLines: 1,
                                  ),
                                  Text(
                                    "Jam " +
                                        DateFormat.Hm().format(e.startTime) +
                                        " - " +
                                        DateFormat.Hm().format(e.endTime),
                                    style: blackTextFont,
                                    overflow: TextOverflow.clip,
                                    maxLines: 1,
                                  ),
                                ],
                              ),
                            ))
                        .toList(),
                  ),
                ],
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 2 + 24,
                      margin: EdgeInsets.only(top: 8),
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 3.2 - 32),
                      height: 40,
                      child: RaisedButton(
                        onPressed: () {},
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        color: orangeColor,
                        child: Text(
                          'Buy',
                          style: blackTextFont,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
