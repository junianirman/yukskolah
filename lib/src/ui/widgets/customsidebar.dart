part of 'widgets.dart';

class CustomSidebar extends StatelessWidget {
  const CustomSidebar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            title: Text(
              "OurProduct",
              style: blackTextFont.copyWith(
                  fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Divider(thickness: 1, color: Colors.black),
          ),
          ListTile(
            title: Text(
              "School",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
          ListTile(
            title: Text(
              "Loan",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
          ListTile(
            title: Text(
              "E-Learning",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
          ListTile(
            title: Text(
              "Consultation",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
          ListTile(
            title: Text(
              "Shopping",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
          ListTile(
            title: Text(
              "Promo",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Divider(thickness: 1, color: Colors.black),
          ),
          ListTile(
            title: Text(
              "Account",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
          ListTile(
            title: Text(
              "About",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
