part of 'widgets.dart';

class ChildrenCard extends StatelessWidget {
  final String title;
  final Children children;
  const ChildrenCard(this.title, this.children, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title, style: blackTextFont),
        title == "Elearning"
            ? Row(
                children: children.elearning
                    .map(
                      (elearning) => Container(
                        height: 80,
                        width: MediaQuery.of(context).size.width / 2.4,
                        padding: EdgeInsets.all(8),
                        margin: EdgeInsets.fromLTRB(
                            0,
                            8,
                            (children.elearning.indexOf(elearning) ==
                                    children.elearning.length - 1
                                ? 0
                                : defaultMargin),
                            8),
                        decoration: BoxDecoration(
                          color: greyColor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(elearning.elearningDate, style: blackTextFont),
                            SizedBox(height: 8),
                            Text(elearning.major, style: blackTextFont)
                          ],
                        ),
                      ),
                    )
                    .toList(),
              )
            : title == "Psycological Test"
                ? Row(
                    children: children.psycological
                        .map(
                          (psycological) => Container(
                            height: 80,
                            width: MediaQuery.of(context).size.width / 2.4,
                            padding: EdgeInsets.all(8),
                            margin: EdgeInsets.fromLTRB(
                                0,
                                8,
                                (children.psycological.indexOf(psycological) ==
                                        children.psycological.length - 1
                                    ? 0
                                    : defaultMargin),
                                8),
                            decoration: BoxDecoration(
                              color: greyColor,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(psycological.psycologicalDate,
                                    style: blackTextFont),
                                SizedBox(height: 8),
                                Text(psycological.name, style: blackTextFont)
                              ],
                            ),
                          ),
                        )
                        .toList(),
                  )
                : title == "Schedule"
                    ? Row(
                        children: children.schedule
                            .map(
                              (schedule) => Container(
                                height: 80,
                                width: MediaQuery.of(context).size.width / 2.4,
                                padding: EdgeInsets.all(8),
                                margin: EdgeInsets.fromLTRB(
                                    0,
                                    8,
                                    (children.schedule.indexOf(schedule) ==
                                            children.schedule.length - 1
                                        ? 0
                                        : defaultMargin),
                                    8),
                                decoration: BoxDecoration(
                                  color: greyColor,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(schedule.scheduleDate,
                                        style: blackTextFont),
                                    SizedBox(height: 8),
                                    Row(
                                      children: schedule.name
                                          .map(
                                            (name) => Text(
                                                name +
                                                    (schedule.name.indexOf(
                                                                name) ==
                                                            schedule.name
                                                                    .length -
                                                                1
                                                        ? ""
                                                        : ". "),
                                                style: blackTextFont),
                                          )
                                          .toList(),
                                    ),
                                  ],
                                ),
                              ),
                            )
                            .toList(),
                      )
                    : title == "Activity"
                        ? Row(
                            children: children.activities
                                .map(
                                  (activity) => Container(
                                    height: 80,
                                    width:
                                        MediaQuery.of(context).size.width / 2.4,
                                    padding: EdgeInsets.all(8),
                                    margin: EdgeInsets.fromLTRB(
                                        0,
                                        8,
                                        (children.activities
                                                    .indexOf(activity) ==
                                                children.activities.length - 1
                                            ? 0
                                            : defaultMargin),
                                        8),
                                    decoration: BoxDecoration(
                                      color: greyColor,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(activity.activitiesDate,
                                            style: blackTextFont),
                                        SizedBox(height: 8),
                                        Text(activity.name,
                                            style: blackTextFont),
                                      ],
                                    ),
                                  ),
                                )
                                .toList(),
                          )
                        : title == "Home Work"
                            ? Row(
                                children: children.homeWork
                                    .map(
                                      (homework) => Container(
                                        height: 80,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2.4,
                                        padding: EdgeInsets.all(8),
                                        margin: EdgeInsets.fromLTRB(
                                            0,
                                            8,
                                            (children.homeWork
                                                        .indexOf(homework) ==
                                                    children.homeWork.length - 1
                                                ? 0
                                                : defaultMargin),
                                            8),
                                        decoration: BoxDecoration(
                                          color: greyColor,
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(homework.homeWorkDate,
                                                style: blackTextFont),
                                            SizedBox(height: 8),
                                            Text(homework.name,
                                                style: blackTextFont),
                                          ],
                                        ),
                                      ),
                                    )
                                    .toList(),
                              )
                            : Row(
                                children: children.book
                                    .map(
                                      (book) => Container(
                                        height: 80,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2.4,
                                        padding: EdgeInsets.all(8),
                                        margin: EdgeInsets.fromLTRB(
                                            0,
                                            8,
                                            (children.book.indexOf(book) ==
                                                    children.book.length - 1
                                                ? 0
                                                : defaultMargin),
                                            8),
                                        decoration: BoxDecoration(
                                          color: greyColor,
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(book.bookDate,
                                                style: blackTextFont),
                                            SizedBox(height: 8),
                                            Text(book.name,
                                                style: blackTextFont),
                                          ],
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
      ],
    );
  }
}
