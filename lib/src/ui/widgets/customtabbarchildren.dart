part of 'widgets.dart';

class CustomTabbarChildren extends StatelessWidget {
  final int selectedIndex;
  final List<Children> children;
  final Function(int) onTap;

  const CustomTabbarChildren(
      {@required this.children, this.selectedIndex, this.onTap, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Stack(
        children: [
          Row(
            children: children
                .map(
                  (e) => GestureDetector(
                    onTap: () {
                      if (onTap != null) {
                        onTap(children.indexOf(e));
                      }
                    },
                    child: Container(
                      height: 85,
                      width: 85,
                      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(32),
                        color: (children.indexOf(e) == selectedIndex)
                            ? Color(0xFFFFFFFF)
                            : greyColor,
                        image: DecorationImage(
                            image: AssetImage(e.picturePath),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                )
                .toList(),
          )
        ],
      ),
    );
  }
}
