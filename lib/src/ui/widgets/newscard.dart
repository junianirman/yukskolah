part of 'widgets.dart';

class NewsCard extends StatelessWidget {
  final News news;
  final Function onTap;

  const NewsCard(this.news, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        height: 150,
        width: (MediaQuery.of(context).size.width - 32),
        margin:
            EdgeInsets.fromLTRB(defaultMargin, defaultMargin, defaultMargin, 0),
        padding: EdgeInsets.all(4),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: greyColor.withOpacity(0.5)),
        child: Row(
          children: [
            Container(
              height: 130,
              width: 130,
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(news.picturePath), fit: BoxFit.cover),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(8),
                child: Text(
                  news.description,
                  style: blackTextFont,
                  textAlign: TextAlign.justify,
                  maxLines: 8,
                  overflow: TextOverflow.clip,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
