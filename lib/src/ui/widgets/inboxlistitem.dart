part of 'widgets.dart';

class InboxListItem extends StatelessWidget {
  final Inbox inbox;
  final double itemWidth;

  const InboxListItem({@required this.inbox, @required this.itemWidth, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width - 16,
      margin:
          EdgeInsets.fromLTRB(defaultMargin, defaultMargin, defaultMargin, 0),
      padding: EdgeInsets.symmetric(vertical: defaultMargin, horizontal: 8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: greyColor.withOpacity(0.5)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 90,
            height: 90,
            margin: EdgeInsets.symmetric(horizontal: 12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                  image: AssetImage(inbox.picturePath), fit: BoxFit.cover),
            ),
          ),
          SizedBox(
            width: itemWidth -
                212, // ((image width)90 + (image to text) 12 + (rating stars)110)
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  inbox.store,
                  style: blackTextFont.copyWith(fontWeight: FontWeight.bold),
                  maxLines: 1,
                  overflow: TextOverflow.clip,
                ),
                Text(
                  inbox.chat,
                  style: blackTextFont.copyWith(fontWeight: FontWeight.w400),
                  maxLines: 1,
                  overflow: TextOverflow.clip,
                ),
              ],
            ),
          ),
          Text(
            inbox.dateTime,
            style: blackTextFont.copyWith(fontSize: 13),
          )
        ],
      ),
    );
  }
}
