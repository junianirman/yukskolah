part of 'widgets.dart';

class ProductButton extends StatelessWidget {
  final String title;
  final Function onTap;

  const ProductButton(this.title, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 4),
            width: 125,
            // width:
            //     (MediaQuery.of(context).size.width - 2 * defaultMargin - 24) / 2,
            height: 125,
            child: Center(
              child: SizedBox(
                  height: 114,
                  child: Image(image: AssetImage(getImageFromGenre(title)))),
            ),
          ),
          Text(
            title,
            style: blackTextFont.copyWith(fontSize: 14),
          )
        ],
      ),
    );
  }

  String getImageFromGenre(String title) {
    switch (title) {
      case "School":
        return "assets/ic_school.png";
        break;
      case "Loan":
        return "assets/ic_loan.png";
        break;
      case "E-Learning":
        return "assets/ic_elearning.png";
        break;
      case "Consultation":
        return "assets/ic_consultation.jpg";
        break;
      case "Shopping":
        return "assets/ic_shopping.png";
        break;
      case "Promo":
        return "assets/ic_promo.jpg";
        break;
      default:
        return "";
    }
  }
}
