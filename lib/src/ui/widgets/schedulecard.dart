part of 'widgets.dart';

class ScheduleCard extends StatelessWidget {
  final Schedule schedule;
  const ScheduleCard(this.schedule, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return schedule.status == ScheduleStatus.children
        ? Container()
        : Container(
            height: schedule.status == ScheduleStatus.reschedule ? 335 : 200,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.fromLTRB(
              defaultMargin,
              defaultMargin,
              defaultMargin,
              (dummySchedule.indexOf(schedule) == dummySchedule.length - 1
                  ? defaultMargin
                  : 0),
            ),
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                color: darkGreyColor, borderRadius: BorderRadius.circular(8)),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      schedule.status == ScheduleStatus.reschedule
                          ? "Reschedule"
                          : schedule.status == ScheduleStatus.schedule
                              ? "Schedule"
                              : "",
                      style: blackTextFont.copyWith(
                          color: schedule.status == ScheduleStatus.reschedule
                              ? orangeColor
                              : schedule.status == ScheduleStatus.schedule
                                  ? Colors.black
                                  : Colors.black,
                          fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(height: 4),
                Expanded(
                  child: Row(
                    children: [
                      (schedule.status == ScheduleStatus.reschedule)
                          ? Column(
                              children: [
                                Container(
                                  height: 150,
                                  width:
                                      MediaQuery.of(context).size.width / 3.2,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    image: DecorationImage(
                                        image: AssetImage(
                                            schedule.consultant.picturePath),
                                        fit: BoxFit.cover),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 3.2,
                                  margin: EdgeInsets.only(top: 8),
                                  height: 40,
                                  child: RaisedButton(
                                    onPressed: () {},
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    color: Color(0xFFFFFFFF),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          'Date',
                                          style: blackTextFont,
                                        ),
                                        Icon(Icons.date_range),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 3.2,
                                  margin: EdgeInsets.only(top: 8),
                                  height: 40,
                                  child: RaisedButton(
                                    onPressed: () {},
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    color: Color(0xFFFFFFFF),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          'Time',
                                          style: blackTextFont,
                                        ),
                                        Icon(Icons.access_time_rounded),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            )
                          : Column(
                              children: [
                                Container(
                                  height: 150,
                                  width:
                                      MediaQuery.of(context).size.width / 3.2,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    image: DecorationImage(
                                        image: AssetImage(
                                            schedule.consultant.picturePath),
                                        fit: BoxFit.cover),
                                  ),
                                ),
                              ],
                            ),
                      SizedBox(width: 8),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(schedule.consultant.name,
                              style: blackTextFont.copyWith(fontSize: 16)),
                          Container(
                            width: MediaQuery.of(context).size.width / 2 + 24,
                            child: Divider(thickness: 1, color: Colors.black),
                          ),
                          Text(
                            schedule.status == ScheduleStatus.reschedule
                                ? "Consultation Booking"
                                : schedule.status == ScheduleStatus.schedule
                                    ? "Consultation Schedule"
                                    : "",
                            style: blackTextFont,
                          ),
                          schedule.status == ScheduleStatus.reschedule
                              ? Container(
                                  width: MediaQuery.of(context).size.width / 2 +
                                      24,
                                  child: Divider(
                                      thickness: 1, color: Colors.black),
                                )
                              : Container(),
                          Text(
                            schedule.status == ScheduleStatus.reschedule
                                ? "Reschedule"
                                : "",
                            style: blackTextFont,
                          ),
                          Text("Jadwal Konsultasi", style: blackTextFont),
                          Column(
                            children: schedule.consultant.setupDateTime
                                .map((e) => Container(
                                      width: MediaQuery.of(context).size.width /
                                              2 +
                                          24,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "${e.day.dayName}",
                                            style: blackTextFont,
                                            overflow: TextOverflow.clip,
                                            maxLines: 1,
                                          ),
                                          Text(
                                            "Jam " +
                                                DateFormat.Hm()
                                                    .format(e.startTime) +
                                                " - " +
                                                DateFormat.Hm()
                                                    .format(e.endTime),
                                            style: blackTextFont,
                                            overflow: TextOverflow.clip,
                                            maxLines: 1,
                                          ),
                                        ],
                                      ),
                                    ))
                                .toList(),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 6),
                (schedule.status == ScheduleStatus.reschedule)
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 3.2,
                            height: 40,
                            child: RaisedButton(
                              onPressed: () {},
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              color: orangeColor,
                              child: Text(
                                'Agree',
                                style: blackTextFont,
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 3.2,
                            height: 40,
                            child: RaisedButton(
                              onPressed: () {},
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              color: greenColor,
                              child: Text(
                                'Cancel',
                                style: blackTextFont,
                              ),
                            ),
                          ),
                        ],
                      )
                    : Container()
              ],
            ),
          );
  }
}
