part of 'pages.dart';

class LoanaPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoanaPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanaPageState createState() => _LoanaPageState();
}

class _LoanaPageState extends State<LoanaPage> {
  TextEditingController limitController = TextEditingController();
  int selectedAmount = 0;
  CreditCard creditCard = CreditCard.yes;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Text("Kartu Kredit",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              height: 50,
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 32,
                    width: 130,
                    child: RadioListTile(
                      title: Text("Ya", style: blackTextFont),
                      activeColor: greenColor,
                      value: CreditCard.yes,
                      groupValue: creditCard,
                      onChanged: (CreditCard value) {
                        setState(() {
                          creditCard = value;
                        });
                      },
                    ),
                  ),
                  Container(
                    height: 32,
                    width: 130,
                    child: RadioListTile(
                      title: Text("Tidak", style: blackTextFont),
                      activeColor: greenColor,
                      value: CreditCard.no,
                      groupValue: creditCard,
                      onChanged: (CreditCard value) {
                        setState(() {
                          creditCard = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: limitController,
                keyboardType: TextInputType.number,
                onChanged: (text) {
                  String temp = '';
                  for (int i = 0; i <= text.length; i++) {
                    temp += text.isDigit(i) ? text[i] : '';
                  }
                  setState(() {
                    selectedAmount = int.tryParse(temp) ?? 0;
                  });
                  limitController.text = NumberFormat.currency(
                          locale: 'id_ID', symbol: 'IDR ', decimalDigits: 0)
                      .format(selectedAmount);
                  limitController.selection = TextSelection.fromPosition(
                      TextPosition(offset: limitController.text.length));
                },
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyTextFont,
                    hintText: 'Limit'),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 8),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoanbPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
