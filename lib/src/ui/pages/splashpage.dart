part of 'pages.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  CurvedAnimation _curve;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    );
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_curve);
    _controller.forward();
    // Timer(
    //   Duration(seconds: 5),
    //   () => Navigator.of(context)
    //       .pushReplacement(MaterialPageRoute(builder: (context) => Landing())),
    // );
    Timer(Duration(seconds: 5),
        () => context.read<PageBloc>().add(GoToMainPage()));
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      body: Stack(
        children: [
          SafeArea(
            child: Center(
              child: FadeTransition(
                opacity: _animation,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Image.asset(
                        'assets/yukskolah_logo.png',
                        fit: BoxFit.contain,
                        scale: 10,
                      ),
                    ),
                    SizedBox(height: 6),
                    Text(
                      "YukSkolah",
                      style: whiteTextFont.copyWith(
                          fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 32),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                      child: Text(
                        "Committed to Excellence in Education",
                        style: whiteTextFont.copyWith(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.italic),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
