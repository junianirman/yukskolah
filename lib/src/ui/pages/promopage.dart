part of 'pages.dart';

class PromoPage extends StatefulWidget {
  PromoPage({Key key}) : super(key: key);

  @override
  _PromoPageState createState() => _PromoPageState();
}

class _PromoPageState extends State<PromoPage> {
  TextEditingController promocodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToMainPage());
        return;
      },
      child: GeneralPage(
        title: "Promo",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.fromLTRB(
                  defaultMargin, defaultMargin, defaultMargin, 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: promocodeController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(
                      vertical: 8, horizontal: defaultMargin),
                  border: InputBorder.none,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 2,
              margin: EdgeInsets.only(top: 16),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {},
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Verify',
                  style: blackTextFont.copyWith(fontSize: 18),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                itemCount: dummyPromo.length,
                itemBuilder: (_, index) => Container(
                  margin: EdgeInsets.only(
                      bottom: (index == dummyPromo.length - 1 ? 100 : 0)),
                  child: PromoCard(
                    dummyPromo[index],
                    onTap: () {
                      context.read<PageBloc>().add(GoToPromoDetailPage(
                          dummyPromo[index], GoToPromoPage()));
                      return;
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
