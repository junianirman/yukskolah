part of 'pages.dart';

class LoanfPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoanfPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanfPageState createState() => _LoanfPageState();
}

class _LoanfPageState extends State<LoanfPage> {
  BankTransfer bankTransfer = BankTransfer.yes;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Text("Apakah gaji anda di transfer melalui bank ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              height: 50,
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(
                  defaultMargin, 0, defaultMargin, defaultMargin),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 32,
                    width: 130,
                    child: RadioListTile(
                      title: Text("Ya", style: blackTextFont),
                      activeColor: greenColor,
                      value: BankTransfer.yes,
                      groupValue: bankTransfer,
                      onChanged: (BankTransfer value) {
                        setState(() {
                          bankTransfer = value;
                        });
                      },
                    ),
                  ),
                  Container(
                    height: 32,
                    width: 130,
                    child: RadioListTile(
                      title: Text("Tidak", style: blackTextFont),
                      activeColor: greenColor,
                      value: BankTransfer.no,
                      groupValue: bankTransfer,
                      onChanged: (BankTransfer value) {
                        setState(() {
                          bankTransfer = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: defaultMargin),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoangPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
