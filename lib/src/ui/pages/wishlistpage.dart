part of 'pages.dart';

class WishlistPage extends StatelessWidget {
  final PageEvent pageEvent;

  const WishlistPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Wishlist",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: dummyProduct
              .take(1)
              .map(
                (e) => Container(
                  height: 130,
                  margin: EdgeInsets.all(defaultMargin),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: darkGreyColor,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Row(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height,
                        width: 150,
                        // width: MediaQuery.of(context).size.width / 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          image: DecorationImage(
                            image: AssetImage(e.picturePath),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 12),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      e.name,
                                      style: blackTextFont,
                                      maxLines: 1,
                                      overflow: TextOverflow.clip,
                                    ),
                                    SizedBox(height: 4),
                                    Text(
                                      NumberFormat.currency(
                                              locale: "id_ID",
                                              decimalDigits: 0,
                                              symbol: "Rp ")
                                          .format(e.price),
                                      style: blackTextFont.copyWith(
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width / 4 +
                                      32,
                                  height: 40,
                                  child: RaisedButton(
                                    onPressed: () {},
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    color: orangeColor,
                                    child: Text(
                                      'Add to Cart',
                                      style: blackTextFont,
                                    ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Icon(Icons.share),
                                    SizedBox(width: 4),
                                    Icon(Icons.delete_forever_rounded),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
