part of 'pages.dart';

class NewsDetailPage extends StatelessWidget {
  final News news;
  final PageEvent pageEvent;

  const NewsDetailPage(this.news, this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "News",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: Column(
          children: [
            Container(
              height: 200,
              width: (MediaQuery.of(context).size.width - 32),
              margin: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(news.picturePath), fit: BoxFit.cover),
              ),
            ),
            Container(
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 8),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Description",
                  style: blackTextFont,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Text(
                news.description,
                style: greyTextFont.copyWith(fontWeight: FontWeight.w400),
                textAlign: TextAlign.justify,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
