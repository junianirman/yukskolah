part of 'pages.dart';

class TransactionPage extends StatefulWidget {
  final PageEvent pageEvent;

  TransactionPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  TextEditingController transactionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Daftar Transaksi",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Container(
          margin: EdgeInsets.all(defaultMargin),
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: mainColor)),
                child: TextField(
                  controller: transactionController,
                  decoration: InputDecoration(
                    hintText: "Search",
                    hintStyle: greyTextFont,
                    contentPadding: EdgeInsets.all(14),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(height: defaultMargin),
              Expanded(
                child: ListView(
                  children: dummyTransaction
                      .map((e) => e.status == "proses"
                          ? TransactionCard(e)
                          : Container())
                      .toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
