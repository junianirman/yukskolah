part of 'pages.dart';

class GeneralPage extends StatelessWidget {
  final String title;
  final Function onBackButtonPressed;
  final Widget child;
  final Color backColor;

  const GeneralPage(
      {this.title = "Title",
      this.onBackButtonPressed,
      this.child,
      this.backColor,
      Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(color: mainColor),
          SafeArea(
            child: Container(
              color: backColor ?? Colors.white,
            ),
          ),
          SafeArea(
            child:
                // ListView(
                //   children: [
                Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                  width: double.infinity,
                  height: 50,
                  // color: Colors.white,
                  color: mainColor,
                  child: Row(
                    children: [
                      onBackButtonPressed != null
                          ? GestureDetector(
                              onTap: () {
                                if (onBackButtonPressed != null) {
                                  onBackButtonPressed();
                                }
                              },
                              child: Container(
                                width: 24,
                                height: 24,
                                margin: EdgeInsets.only(right: 8),
                                // decoration: BoxDecoration(
                                //   image: DecorationImage(
                                //     image:
                                //         AssetImage('assets/back_arrow.png'),
                                //   ),
                                // ),
                                child: Icon(Icons.arrow_back_ios),
                              ),
                            )
                          : SizedBox(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            title,
                            style: GoogleFonts.raleway(
                                fontSize: 20, fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            //   ],
            // ),
          ),
          SafeArea(
            child: Container(
                margin: EdgeInsets.only(top: 50),
                width: double.infinity,
                color: Color(0xFFFAFAFC),
                child: child ?? SizedBox()),
          ),
        ],
      ),
    );
  }
}
