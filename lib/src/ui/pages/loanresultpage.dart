part of 'pages.dart';

class LoanResultPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoanResultPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanResultPageState createState() => _LoanResultPageState();
}

class _LoanResultPageState extends State<LoanResultPage> {
  TextEditingController namaController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController nomorhpController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: ListView(children: dummyLoan.map((e) => LoanCard(e)).toList()),
      ),
    );
  }
}
