part of 'pages.dart';

class LoankPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoankPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoankPageState createState() => _LoankPageState();
}

class _LoankPageState extends State<LoankPage> {
  List<String> tenor;
  String selectedTenor;

  @override
  void initState() {
    super.initState();
    tenor = [
      '2 bulan',
      '3 bulan',
      '4 bulan',
      '5 bulan',
      '6 bulan',
      '7 bulan',
      '8 bulan',
      '9 bulan',
      '10 bulan',
      '11 bulan',
      '12 bulan',
    ];
    // selectedTenor = tenor[0];
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text("Tenor pinjaman ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: DropdownButton(
                  value: selectedTenor,
                  hint: Text("Tenor", style: greyTextFont),
                  isExpanded: true,
                  underline: SizedBox(),
                  items: tenor
                      .map((e) => DropdownMenuItem(
                          value: e,
                          child: Text(
                            e,
                            style: blackTextFont,
                          )))
                      .toList(),
                  onChanged: (item) {
                    setState(() {
                      selectedTenor = item;
                    });
                  }),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 8),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoanlPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
