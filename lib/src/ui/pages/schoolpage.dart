part of 'pages.dart';

class SchoolPage extends StatefulWidget {
  SchoolPage({Key key}) : super(key: key);

  @override
  _SchoolPageState createState() => _SchoolPageState();
}

class _SchoolPageState extends State<SchoolPage> {
  TextEditingController schoolController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToMainPage());
        return;
      },
      child: GeneralPage(
        title: "School",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.fromLTRB(
                  defaultMargin, defaultMargin, defaultMargin, 8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: schoolController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Search"),
              ),
            ),
            Container(
              height: 60,
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Column(
                children: [
                  Divider(color: mainColor, thickness: 1),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Icon(Icons.sort, size: 14),
                          Text(
                            "Atur",
                            style: blackTextFont.copyWith(fontSize: 12),
                          )
                        ],
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: defaultMargin),
                        child: Text(
                          '|',
                          textAlign: TextAlign.center,
                          style: blackTextFont.copyWith(
                              fontSize: 22,
                              fontWeight: FontWeight.w600,
                              color: mainColor),
                        ),
                      ),
                      Column(
                        children: [
                          Icon(Icons.filter_alt_outlined, size: 14),
                          Text(
                            "Filter",
                            style: blackTextFont.copyWith(fontSize: 12),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Divider(color: mainColor, thickness: 1)
                ],
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  Container(
                    margin: EdgeInsets.all(defaultMargin),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Kindergarten & Preschool",
                          style: blackTextFont.copyWith(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                            onTap: () {
                              context.read<PageBloc>().add(GoToSeeallSchoolPage(
                                  "Kindergarten & Preschool",
                                  SchoolType.kindergarten,
                                  dummySchool,
                                  GoToSchoolPage()));
                              // .add(GoToKindergartenSchoolPage(dummySchool));
                            },
                            child: Text("See all")),
                      ],
                    ),
                  ),
                  Row(
                    children: dummySchool
                        .where((element) =>
                            element.types == SchoolType.kindergarten)
                        .take(2)
                        .map(
                          (e) => Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: SchoolCard(
                              e,
                              onTap: () {
                                context.read<PageBloc>().add(
                                    GoToSchoolDetailPage(e, GoToSchoolPage()));
                              },
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  Container(
                    margin: EdgeInsets.all(defaultMargin),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Elementary School",
                          style: blackTextFont.copyWith(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                            onTap: () {
                              context.read<PageBloc>().add(GoToSeeallSchoolPage(
                                  "Elementary School",
                                  SchoolType.elementary_school,
                                  dummySchool,
                                  GoToSchoolPage()));
                              // .add(GoToElementarySchoolPage(dummySchool));
                            },
                            child: Text("See all")),
                      ],
                    ),
                  ),
                  Row(
                    children: dummySchool
                        .where((element) =>
                            element.types == SchoolType.elementary_school)
                        .take(2)
                        .map(
                          (e) => Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: SchoolCard(
                              e,
                              onTap: () {
                                context.read<PageBloc>().add(
                                    GoToSchoolDetailPage(e, GoToSchoolPage()));
                              },
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  Container(
                    margin: EdgeInsets.all(defaultMargin),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Junior High School",
                          style: blackTextFont.copyWith(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                            onTap: () {
                              context.read<PageBloc>().add(GoToSeeallSchoolPage(
                                  "Junior High School",
                                  SchoolType.junior_high_school,
                                  dummySchool,
                                  GoToSchoolPage()));
                              // .add(GoToJuniorHighSchoolPage(dummySchool));
                            },
                            child: Text("See all")),
                      ],
                    ),
                  ),
                  Row(
                    children: dummySchool
                        .where((element) =>
                            element.types == SchoolType.junior_high_school)
                        .take(2)
                        .map(
                          (e) => Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: SchoolCard(
                              e,
                              onTap: () {
                                context.read<PageBloc>().add(
                                    GoToSchoolDetailPage(e, GoToSchoolPage()));
                              },
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  Container(
                    margin: EdgeInsets.all(defaultMargin),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Senior High School",
                          style: blackTextFont.copyWith(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                            onTap: () {
                              context.read<PageBloc>().add(GoToSeeallSchoolPage(
                                  "Senior High School",
                                  SchoolType.senior_high_school,
                                  dummySchool,
                                  GoToSchoolPage()));
                              // .add(GoToSeniorHighSchoolPage(dummySchool));
                            },
                            child: Text("See all")),
                      ],
                    ),
                  ),
                  Row(
                    children: dummySchool
                        .where((element) =>
                            element.types == SchoolType.senior_high_school)
                        .take(2)
                        .map(
                          (e) => Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: SchoolCard(
                              e,
                              onTap: () {
                                context.read<PageBloc>().add(
                                    GoToSchoolDetailPage(e, GoToSchoolPage()));
                              },
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  SizedBox(height: defaultMargin),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
