part of 'pages.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  bool isSelectedAll = false;
  bool isSelectedStore = false;
  bool isSelectedProduct = false;

  int quantity = 1;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Container(
              height: 32,
              margin: EdgeInsets.all(defaultMargin),
              child: CheckboxListTile(
                  title: Text("Pilih semua produk", style: blackTextFont),
                  controlAffinity: ListTileControlAffinity.leading,
                  value: isSelectedAll,
                  onChanged: (value) {
                    setState(() {
                      isSelectedAll = value;
                      isSelectedStore = value;
                      isSelectedProduct = value;
                    });
                  }),
            ),
            Expanded(
              child: ListView(
                children: [
                  Container(
                    height: 175,
                    margin: EdgeInsets.all(defaultMargin),
                    decoration: BoxDecoration(
                        color: darkGreyColor,
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      children: [
                        Container(
                          // height: 48,
                          child: CheckboxListTile(
                              title: Text("Toko Sepatu Serba Ada",
                                  style: blackTextFont),
                              controlAffinity: ListTileControlAffinity.leading,
                              value: isSelectedStore,
                              onChanged: (value) {
                                setState(() {
                                  isSelectedStore = value;
                                  isSelectedProduct = value;
                                });
                              }),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 24,
                                child: Checkbox(
                                  value: isSelectedProduct,
                                  onChanged: (value) {
                                    setState(() {
                                      isSelectedProduct = value;
                                    });
                                  },
                                ),
                              ),
                              SizedBox(width: 12),
                              Container(
                                height: 100,
                                width: 120,
                                // margin: EdgeInsets.only(top: 12),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  image: DecorationImage(
                                      image: AssetImage('assets/shoes_1.jpg'),
                                      fit: BoxFit.cover),
                                ),
                              ),
                              SizedBox(width: 8),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 2.4,
                                    child: Text(dummyProduct[0].name,
                                        overflow: TextOverflow.clip,
                                        maxLines: 1,
                                        style: blackTextFont),
                                  ),
                                  Text(
                                    NumberFormat.currency(
                                            locale: "id_ID",
                                            decimalDigits: 0,
                                            symbol: "Rp ")
                                        .format(dummyProduct[0].price),
                                    style: blackTextFont.copyWith(
                                        fontWeight: FontWeight.w400),
                                  ),
                                  SizedBox(height: 16),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            quantity = max(1, quantity - 1);
                                          });
                                        },
                                        child: Container(
                                          width: 26,
                                          height: 26,
                                          child: Text(
                                            "-",
                                            style: blackTextFont.copyWith(
                                                fontSize: 24),
                                            // textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 32,
                                        child: Text(
                                          quantity.toString(),
                                          textAlign: TextAlign.center,
                                          style: blackTextFont.copyWith(
                                              fontSize: 18),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            quantity = min(999, quantity + 1);
                                          });
                                        },
                                        child: Container(
                                          width: 26,
                                          height: 26,
                                          child: Text(
                                            "+",
                                            style: blackTextFont.copyWith(
                                                fontSize: 24),
                                            textAlign: TextAlign.end,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            32,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [Icon(Icons.delete)],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text("Total($quantity)", style: blackTextFont),
                      Text(
                        NumberFormat.currency(
                                locale: "id_ID",
                                decimalDigits: 0,
                                symbol: "Rp ")
                            .format(dummyProduct[0].price * quantity),
                        style:
                            blackTextFont.copyWith(fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2 - 32,
                    height: 45,
                    padding: EdgeInsets.only(left: 32),
                    child: RaisedButton(
                      onPressed: () {},
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      color: orangeColor,
                      child: Text(
                        'Checkout',
                        style: blackTextFont.copyWith(fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 75)
          ],
        ),
      ),
    );
  }
}
