part of 'pages.dart';

class EventDetailPage extends StatelessWidget {
  final Event event;
  final PageEvent pageEvent;

  const EventDetailPage(this.event, this.pageEvent, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Event",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              width: (MediaQuery.of(context).size.width - 32),
              margin: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(event.picturePath), fit: BoxFit.cover),
              ),
            ),
            Container(
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 8),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  event.title,
                  style: blackTextFont,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 0, defaultMargin, 24),
              child: Text("Tempat : " + event.place,
                  style: blackTextFont.copyWith(fontWeight: FontWeight.w400)),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(
                  defaultMargin, defaultMargin, defaultMargin, 24),
              child: Text(
                  "Biaya pendaftaran " +
                      NumberFormat.currency(
                              locale: "id_ID", decimalDigits: 0, symbol: "Rp ")
                          .format(event.price),
                  style: blackTextFont.copyWith(fontWeight: FontWeight.w400)),
            ),
          ],
        ),
      ),
    );
  }
}
