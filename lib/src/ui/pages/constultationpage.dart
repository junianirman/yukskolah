part of 'pages.dart';

class ConsultationPage extends StatefulWidget {
  ConsultationPage({Key key}) : super(key: key);

  @override
  _ConsultationPageState createState() => _ConsultationPageState();
}

class _ConsultationPageState extends State<ConsultationPage> {
  int selectedIndex = 0;
  TextEditingController consultationController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToMainPage());
        return;
      },
      child: GeneralPage(
        title: "Consultation",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          children: [
            selectedIndex == 2
                ? Container()
                : CustomTabbar(
                    titles: ['Consultation', 'Psycological Test', 'Schedule'],
                    selectedIndex: selectedIndex,
                    onTap: (index) {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  ),
            selectedIndex == 2
                ? Container()
                : Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.fromLTRB(
                        defaultMargin, defaultMargin, defaultMargin, 4),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mainColor)),
                    child: TextField(
                      controller: consultationController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 8, horizontal: defaultMargin),
                          border: InputBorder.none,
                          hintText: "Search"),
                    ),
                  ),
            selectedIndex == 2
                ? Container()
                : Container(
                    height: 60,
                    width: double.infinity,
                    margin:
                        EdgeInsets.fromLTRB(defaultMargin, 0, defaultMargin, 0),
                    child: Column(
                      children: [
                        Divider(color: mainColor, thickness: 1),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                Icon(Icons.sort, size: 14),
                                Text(
                                  "Atur",
                                  style: blackTextFont.copyWith(fontSize: 12),
                                )
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: defaultMargin),
                              child: Text(
                                '|',
                                textAlign: TextAlign.center,
                                style: blackTextFont.copyWith(
                                    fontSize: 22,
                                    fontWeight: FontWeight.w600,
                                    color: mainColor),
                              ),
                            ),
                            Column(
                              children: [
                                Icon(Icons.filter_alt_outlined, size: 14),
                                Text(
                                  "Filter",
                                  style: blackTextFont.copyWith(fontSize: 12),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Divider(color: mainColor, thickness: 1)
                      ],
                    ),
                  ),
            Expanded(
              child: ListView(
                children: (selectedIndex == 0)
                    ? dummyConsultant.map((e) => ConsultationCard(e)).toList()
                    : (selectedIndex == 1)
                        ? dummyPsycological
                            .map((e) => PsycologicalCard(e))
                            .toList()
                        : dummySchedule.map((e) => ScheduleCard(e)).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
