part of 'pages.dart';

class LoangPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoangPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoangPageState createState() => _LoangPageState();
}

class _LoangPageState extends State<LoangPage> {
  List<String> bank;
  String selectedBank;

  @override
  void initState() {
    super.initState();
    bank = [
      'BCA',
      'BNI',
      'Bank Mandiri',
      'Bank Mega',
      'CIMB Niaga',
      'BRI',
      'dan list bank lainya'
    ];
    // selectedPekerjaan = pekerjaan[0];
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text("Bank apa yang anda gunakan untuk menerima gaji ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: DropdownButton(
                  value: selectedBank,
                  hint: Text("Bank", style: greyTextFont),
                  isExpanded: true,
                  underline: SizedBox(),
                  items: bank
                      .map((e) => DropdownMenuItem(
                          value: e,
                          child: Text(
                            e,
                            style: blackTextFont,
                          )))
                      .toList(),
                  onChanged: (item) {
                    setState(() {
                      selectedBank = item;
                    });
                  }),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 8),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoanhPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
