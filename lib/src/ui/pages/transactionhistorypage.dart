part of 'pages.dart';

class TransactionHistoryPage extends StatefulWidget {
  final PageEvent pageEvent;

  TransactionHistoryPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _TransactionHistoryPageState createState() => _TransactionHistoryPageState();
}

class _TransactionHistoryPageState extends State<TransactionHistoryPage> {
  TextEditingController transactionHistoryController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Histori Transaksi",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Container(
          margin: EdgeInsets.all(defaultMargin),
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: mainColor)),
                child: TextField(
                  controller: transactionHistoryController,
                  decoration: InputDecoration(
                    hintText: "Search",
                    hintStyle: greyTextFont,
                    contentPadding: EdgeInsets.all(14),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(height: defaultMargin),
              Expanded(
                child: ListView(
                  children: dummyTransaction
                      .map((e) => e.status == "selesai"
                          ? TransactionCard(e)
                          : Container())
                      .toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
