part of 'pages.dart';

class ElearningMaterialPage extends StatelessWidget {
  final Elearning elearning;
  final PageEvent pageEvent;

  const ElearningMaterialPage(this.elearning, this.pageEvent, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "E-learning",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            Container(
              height: 250,
              width: (MediaQuery.of(context).size.width - 32),
              margin: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(elearning.picturePath),
                    fit: BoxFit.cover),
              ),
            ),
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    elearning.major,
                    style: blackTextFont.copyWith(fontSize: 16),
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                  Text(
                    "Kelas : " + "${elearning.kelas}",
                    style: blackTextFont.copyWith(fontSize: 16),
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Kurikulum : " + elearning.curriculum,
                        style: blackTextFont.copyWith(fontSize: 16),
                        maxLines: 1,
                        overflow: TextOverflow.clip,
                      ),
                      Text(
                        NumberFormat.currency(
                                locale: "id_ID",
                                decimalDigits: 0,
                                symbol: "Rp ")
                            .format(elearning.price),
                        style: blackTextFont.copyWith(fontSize: 16),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Column(
                children: elearning.material
                    .map(
                      (e) => ElearningMaterialCard(e, onTap: () {
                        context.read<PageBloc>().add(GoToElearningDetailPage(
                            GoToElearningMaterialPage(elearning, pageEvent)));
                      }),
                    )
                    .toList()),
            SizedBox(height: defaultMargin)
          ],
        ),
      ),
    );
  }
}
