part of 'pages.dart';

class AccountPage extends StatelessWidget {
  const AccountPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 150,
                  height: 150,
                  margin: EdgeInsets.only(top: 32),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(dummyUser.picturePath),
                        fit: BoxFit.cover),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 2 * defaultMargin,
                  child: Text(
                    dummyUser.name,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.clip,
                    style: blackTextFont.copyWith(fontSize: 18),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      // width: MediaQuery.of(context).size.width / 3,
                      // margin: EdgeInsets.only(top: 8, bottom: 30),
                      child: Text(
                        dummyUser.email,
                        textAlign: TextAlign.start,
                        style: greyTextFont.copyWith(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      child: Text(
                        '|',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 24),
                      ),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 4,
                      // margin: EdgeInsets.only(top: 8, bottom: 30),
                      child: Text(
                        dummyUser.phoneNumber,
                        textAlign: TextAlign.end,
                        style: greyTextFont.copyWith(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: 24, vertical: defaultMargin),
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: greyColor.withAlpha(50),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Balance',
                        style: blackTextFont.copyWith(fontSize: 16),
                      ),
                      SizedBox(height: 4),
                      Text(
                        NumberFormat.currency(
                                locale: "id_ID",
                                decimalDigits: 0,
                                symbol: "Rp ")
                            .format(dummyUser.balance),
                        style: blackTextFont.copyWith(
                            fontSize: 24, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Stack(
              children: [
                SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child: Column(
                      children: [
                        GestureDetector(
                          child: ListTile(
                            leading: Icon(Icons.person),
                            title: Text(
                              'Profile',
                              style: blackTextFont.copyWith(
                                  fontWeight: FontWeight.w600),
                            ),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          onTap: () {
                            context.read<PageBloc>().add(GoToAccountDetailPage(
                                dummyUser, GoToMainPage(bottomNavBarIndex: 4)));
                          },
                        ),
                        GestureDetector(
                          child: ListTile(
                            leading: Icon(Icons.people),
                            title: Text(
                              'Children',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          onTap: () {
                            context.read<PageBloc>().add(GoToChildrenPage(
                                GoToMainPage(bottomNavBarIndex: 4)));
                          },
                        ),
                        GestureDetector(
                          child: ListTile(
                            leading: Icon(Icons.monetization_on),
                            title: Text(
                              'Whislist',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          onTap: () {
                            context.read<PageBloc>().add(GoToWishlistPage(
                                GoToMainPage(bottomNavBarIndex: 4)));
                          },
                        ),
                        GestureDetector(
                          child: ListTile(
                            leading: Icon(Icons.shopping_cart),
                            title: Text(
                              'Daftar Transaksi',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          onTap: () {
                            context.read<PageBloc>().add(GoToTransactionPage(
                                GoToMainPage(bottomNavBarIndex: 4)));
                          },
                        ),
                        GestureDetector(
                          child: ListTile(
                            leading: Icon(Icons.history),
                            title: Text(
                              'Histori Transaksi',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          onTap: () {
                            context.read<PageBloc>().add(
                                GoToTransactionHistoryPage(
                                    GoToMainPage(bottomNavBarIndex: 4)));
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          child: ListTile(
                            title: RaisedButton(
                              onPressed: () {},
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              color: accentColor2,
                              splashColor: accentColor1,
                              textColor: accentColor1,
                              padding: EdgeInsets.all(16),
                              child: Center(
                                child: Text('LOGOUT',
                                    style: TextStyle(fontSize: 18)),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
