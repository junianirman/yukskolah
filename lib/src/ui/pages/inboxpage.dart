part of 'pages.dart';

class InboxPage extends StatefulWidget {
  const InboxPage({Key key}) : super(key: key);

  @override
  _InboxPageState createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    double listItemWidth =
        MediaQuery.of(context).size.width - 2 * defaultMargin;

    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            CustomTabbar(
              titles: [
                'School',
                'Loan',
                'Consultation',
                'E-Learning',
                'Shopping'
              ],
              selectedIndex: selectedIndex,
              onTap: (index) {
                setState(() {
                  selectedIndex = index;
                });
              },
            ),
            Expanded(
              child: ListView(
                children: dummyInbox
                    .where((element) => element.inboxType == InboxType.school
                        ? selectedIndex == 0
                        : element.inboxType == InboxType.loan
                            ? selectedIndex == 1
                            : element.inboxType == InboxType.consultation
                                ? selectedIndex == 2
                                : element.inboxType == InboxType.elearning
                                    ? selectedIndex == 3
                                    : selectedIndex == 4)
                    .toList()
                    .map(
                      (e) => GestureDetector(
                          onTap: () {
                            context.read<PageBloc>().add(GoToChatPage(
                                e, GoToMainPage(bottomNavBarIndex: 2)));
                          },
                          child: InboxListItem(
                              inbox: e, itemWidth: listItemWidth)),
                    )
                    .toList(),
              ),
            ),
            SizedBox(height: 75)
          ],
        ),
      ),
    );
  }
}
