part of 'pages.dart';

class SchoolDetailPage extends StatelessWidget {
  final School school;
  final PageEvent pageEvent;

  const SchoolDetailPage(this.school, this.pageEvent, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "School",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            Container(
              height: 200,
              width: (MediaQuery.of(context).size.width - 32),
              margin: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(school.picturePath), fit: BoxFit.cover),
              ),
            ),
            Container(
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 4),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  school.name,
                  style: blackTextFont.copyWith(
                      fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Text(
                "Akreditasi " +
                    school.accreditation +
                    " passing Grade " +
                    "${school.minGrade}" +
                    "-" +
                    "${school.maxGrade}",
                style: blackTextFont.copyWith(fontWeight: FontWeight.w400),
              ),
            ),
            SizedBox(height: 32),
            Container(
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 4),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Description",
                  style: blackTextFont.copyWith(
                      fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Text(
                school.description,
                style: blackTextFont.copyWith(fontWeight: FontWeight.w400),
                textAlign: TextAlign.justify,
              ),
            ),
            SizedBox(height: 16),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Row(
                children: [
                  Text("Biaya Pendaftaran ", style: blackTextFont),
                  Text(
                    NumberFormat.currency(
                            locale: "id_ID", decimalDigits: 0, symbol: "Rp ")
                        .format(school.price),
                    style: blackTextFont,
                  ),
                ],
              ),
            ),
            SizedBox(height: 32),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Facilities",
                  style: blackTextFont.copyWith(
                      fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              height: 150,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  Row(
                    children: dummyFacilities
                        .map(
                          (e) => Padding(
                            padding: EdgeInsets.only(
                                left: (e == dummyFacilities.first)
                                    ? defaultMargin
                                    : 0,
                                right: defaultMargin),
                            child: FacilitiesCard(e),
                          ),
                        )
                        .toList(),
                  )
                ],
              ),
            ),
            SizedBox(height: 8),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Activity",
                  style: blackTextFont.copyWith(
                      fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              height: 150,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  Row(
                    children: dummyActivities
                        .map(
                          (e) => Padding(
                            padding: EdgeInsets.only(
                                left: (e == dummyActivities.first)
                                    ? defaultMargin
                                    : 0,
                                right: defaultMargin),
                            child: ActivitiesCard(e),
                          ),
                        )
                        .toList(),
                  )
                ],
              ),
            ),
            SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 2 - 24,
                  height: 45,
                  padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: RaisedButton(
                    onPressed: () {
                      context.read<PageBloc>().add(GoToSchoolInformationPage(
                          school, GoToSchoolDetailPage(school, pageEvent)));
                    },
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    color: orangeColor,
                    child: Text(
                      'Information',
                      style: blackTextFont.copyWith(fontSize: 18),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2 - 24,
                  height: 45,
                  padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: RaisedButton(
                    onPressed: () {
                      context.read<PageBloc>().add(GoToSchoolRegistrationPage(
                          school, GoToSchoolDetailPage(school, pageEvent)));
                    },
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    color: orangeColor,
                    child: Text(
                      'Register',
                      style: blackTextFont.copyWith(fontSize: 18),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: defaultMargin)
          ],
        ),
      ),
    );
  }
}
