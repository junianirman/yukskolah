part of 'pages.dart';

class SchoolInformationPage extends StatelessWidget {
  final School school;
  final PageEvent pageEvent;

  const SchoolInformationPage(this.school, this.pageEvent, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Information",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Text(
                school.information,
                style: blackTextFont,
                textAlign: TextAlign.justify,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
