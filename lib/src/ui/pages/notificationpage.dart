part of 'pages.dart';

class NotificationPage extends StatelessWidget {
  final PageEvent pageEvent;

  const NotificationPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Notifikasi",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: ListView(
          children: dummyNotification
              .map(
                (e) => NotificationCard(e),
              )
              .toList(),
        ),
      ),
    );
  }
}
