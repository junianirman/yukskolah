part of 'pages.dart';

class ShoppingDetailPage extends StatefulWidget {
  final Product product;
  final PageEvent pageEvent;

  const ShoppingDetailPage(this.product, this.pageEvent, {Key key})
      : super(key: key);

  @override
  _ShoppingDetailPageState createState() => _ShoppingDetailPageState();
}

class _ShoppingDetailPageState extends State<ShoppingDetailPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Shopping",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              height: 200,
              width: (MediaQuery.of(context).size.width - 32),
              margin: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(widget.product.picturePath),
                    fit: BoxFit.cover),
              ),
            ),
            Container(
              height: 65,
              width: (MediaQuery.of(context).size.width - 32),
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: greyColor.withOpacity(0.5)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    widget.product.name,
                    style: blackTextFont,
                    textAlign: TextAlign.justify,
                    maxLines: 8,
                    overflow: TextOverflow.clip,
                  ),
                  Text(
                      NumberFormat.currency(
                              locale: "id_ID", decimalDigits: 0, symbol: "Rp ")
                          .format(widget.product.price),
                      style: blackTextFont)
                ],
              ),
            ),
            SizedBox(height: 8),
            Container(
              height: MediaQuery.of(context).size.height / 2 + defaultMargin,
              child: Column(
                children: [
                  CustomTabbar(
                    titles: [
                      'Varian',
                      'Deskripsi',
                      'Seller',
                      'Pengiriman',
                      'Promo',
                      'Ulasan'
                    ],
                    selectedIndex: selectedIndex,
                    onTap: (index) {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  ),
                  Expanded(
                    child: selectedIndex == 0
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: defaultMargin),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Warna",
                                    style: blackTextFont,
                                  ),
                                ),
                              ),
                              SizedBox(height: 8),
                              Container(
                                height: 100,
                                width: double.infinity,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: [
                                    Row(
                                      children: widget.product.varian
                                          .map(
                                            (e) => Padding(
                                              padding: EdgeInsets.only(
                                                  left: (e ==
                                                          widget.product.varian
                                                              .first)
                                                      ? defaultMargin
                                                      : 0,
                                                  right: defaultMargin),
                                              child: Container(
                                                height: 100,
                                                width: 100,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          e.picturePath),
                                                      fit: BoxFit.cover),
                                                ),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(height: 24),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: defaultMargin),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Ukuran",
                                    style: blackTextFont,
                                  ),
                                ),
                              ),
                              SizedBox(height: 8),
                              Container(
                                height: 50,
                                width: double.infinity,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: [
                                    Row(
                                      children: widget.product.size
                                          .map(
                                            (e) => Padding(
                                              padding: EdgeInsets.only(
                                                  left: (e ==
                                                          widget.product.size
                                                              .first)
                                                      ? defaultMargin
                                                      : 0,
                                                  right: defaultMargin),
                                              child: Container(
                                                height: 30,
                                                width: 40,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(4),
                                                  color: greyColor
                                                      .withOpacity(0.5),
                                                ),
                                                child: Center(
                                                  child: Text("$e",
                                                      style: blackTextFont),
                                                ),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          )
                        : selectedIndex == 1
                            ? Container(
                                height: MediaQuery.of(context).size.height / 2 -
                                    defaultMargin,
                                margin: EdgeInsets.all(defaultMargin),
                                child: Center(child: Text("Description")),
                              )
                            : selectedIndex == 2
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 150,
                                        width: 150,
                                        margin: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          image: DecorationImage(
                                              image: AssetImage(widget
                                                  .product.seller.picturePath),
                                              fit: BoxFit.cover),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(8),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              widget.product.seller.name,
                                              style: blackTextFont.copyWith(
                                                  fontSize: 16),
                                              textAlign: TextAlign.justify,
                                              maxLines: 1,
                                              overflow: TextOverflow.clip,
                                            ),
                                            SizedBox(height: 6),
                                            RatingStars(
                                                widget.product.seller.rate),
                                            SizedBox(height: 6),
                                            Row(
                                              children: [
                                                Icon(Icons.location_on_rounded,
                                                    size: 24),
                                                Text(
                                                    widget
                                                        .product.seller.address,
                                                    style: blackTextFont,
                                                    textAlign:
                                                        TextAlign.justify,
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.clip),
                                              ],
                                            ),
                                            SizedBox(height: 6),
                                            Container(
                                              width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      4 -
                                                  defaultMargin,
                                              height: 40,
                                              child: RaisedButton(
                                                onPressed: () {},
                                                elevation: 0,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                color: darkGreyColor,
                                                child: Text(
                                                  'Ikuti',
                                                  style: blackTextFont.copyWith(
                                                      fontSize: 16),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                : selectedIndex == 3
                                    ? Center(
                                        child: Text(
                                          "Cek Pengiriman ke Lokasimu",
                                          style: blackTextFont.copyWith(
                                              fontSize: 16),
                                          textAlign: TextAlign.justify,
                                          maxLines: 1,
                                          overflow: TextOverflow.clip,
                                        ),
                                      )
                                    : selectedIndex == 4
                                        ? Center(
                                            child: Container(
                                              height: 120,
                                              margin:
                                                  EdgeInsets.all(defaultMargin),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  color: darkGreyColor),
                                              child: Container(
                                                margin: EdgeInsets.all(
                                                    defaultMargin),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.all(8),
                                                      child: Text(
                                                        "Diskon ${widget.product.discount}%",
                                                        style: blackTextFont
                                                            .copyWith(
                                                                fontSize: 16),
                                                        textAlign:
                                                            TextAlign.justify,
                                                        maxLines: 1,
                                                        overflow:
                                                            TextOverflow.clip,
                                                      ),
                                                    ),
                                                    SizedBox(height: 6),
                                                    Divider(
                                                        thickness: 1,
                                                        color: Colors.black),
                                                    SizedBox(height: 6),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        SelectableText(
                                                          widget.product
                                                              .promoCode,
                                                          style: blackTextFont,
                                                          textAlign:
                                                              TextAlign.justify,
                                                          maxLines: 1,
                                                          onTap: () {
                                                            Clipboard.setData(
                                                                ClipboardData(
                                                                    text: widget
                                                                        .product
                                                                        .promoCode));
                                                          },
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            Clipboard.setData(ClipboardData(
                                                                    text: widget
                                                                        .product
                                                                        .promoCode))
                                                                .then(
                                                              (_) =>
                                                                  Get.snackbar(
                                                                "",
                                                                "",
                                                                backgroundColor:
                                                                    accentColor2,
                                                                icon: Icon(
                                                                    MdiIcons
                                                                        .closeCircleOutline,
                                                                    color: Colors
                                                                        .white),
                                                                titleText: (Text(
                                                                    "Copied",
                                                                    style: whiteTextFont.copyWith(
                                                                        fontWeight:
                                                                            FontWeight.w600))),
                                                                messageText: Text(
                                                                    widget
                                                                        .product
                                                                        .promoCode,
                                                                    style:
                                                                        whiteTextFont),
                                                              ),
                                                            );
                                                          },
                                                          child: Text("salin",
                                                              style:
                                                                  blackTextFont,
                                                              textAlign:
                                                                  TextAlign
                                                                      .justify,
                                                              maxLines: 1,
                                                              overflow:
                                                                  TextOverflow
                                                                      .clip),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          )
                                        : selectedIndex == 5
                                            ? Column(
                                                children: [
                                                  widget.product.ulasan
                                                      .take(1)
                                                      .map(
                                                        (e) => Container(
                                                          margin: EdgeInsets.all(
                                                              defaultMargin),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Align(
                                                                alignment:
                                                                    Alignment
                                                                        .topLeft,
                                                                child: Text(
                                                                  "Ulasan Barang",
                                                                  style:
                                                                      blackTextFont,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  height: 8),
                                                              Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Container(
                                                                    height: 80,
                                                                    width: 80,
                                                                    margin: EdgeInsets
                                                                        .all(8),
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              8),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(e
                                                                              .user
                                                                              .picturePath),
                                                                          fit: BoxFit
                                                                              .cover),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    margin: EdgeInsets
                                                                        .all(8),
                                                                    child:
                                                                        Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .center,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          e.user
                                                                              .name,
                                                                          style:
                                                                              blackTextFont.copyWith(fontSize: 16),
                                                                          textAlign:
                                                                              TextAlign.justify,
                                                                          maxLines:
                                                                              1,
                                                                          overflow:
                                                                              TextOverflow.clip,
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                6),
                                                                        RatingStars(
                                                                            e.rate),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              SizedBox(
                                                                  height: 6),
                                                              Text(e.ulasan,
                                                                  style:
                                                                      blackTextFont,
                                                                  textAlign:
                                                                      TextAlign
                                                                          .justify,
                                                                  maxLines: 1,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .clip),
                                                              SizedBox(
                                                                  height: 6),
                                                              Container(
                                                                height: 60,
                                                                width: MediaQuery.of(context)
                                                                            .size
                                                                            .width /
                                                                        4 -
                                                                    defaultMargin,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              8),
                                                                  image: DecorationImage(
                                                                      image: AssetImage(e
                                                                          .picturePath),
                                                                      fit: BoxFit
                                                                          .contain),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                      .first,
                                                  SizedBox(height: 32),
                                                  Text("Lihat semua ulasan",
                                                      style: blackTextFont,
                                                      textAlign:
                                                          TextAlign.justify,
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.clip),
                                                ],
                                              )
                                            : Container(),
                  ),
                  Container(
                    height: 50,
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 4,
                          height: 40,
                          child: RaisedButton(
                            onPressed: () {
                              context.read<PageBloc>().add(GoToChatPage(
                                  null,
                                  GoToShoppingDetailPage(
                                      widget.product, widget.pageEvent)));
                            },
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            color: darkGreyColor,
                            child: Text(
                              'Chat',
                              style: blackTextFont,
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 4,
                          height: 40,
                          child: RaisedButton(
                            onPressed: () {},
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            color: darkGreyColor,
                            child: Text(
                              'Add to Cart',
                              style: blackTextFont,
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 4,
                          height: 40,
                          child: RaisedButton(
                            onPressed: () {},
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            color: darkGreyColor,
                            child: Text(
                              'Buy Now',
                              style: blackTextFont,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
