part of 'pages.dart';

class LoanbPage extends StatefulWidget {
  final PageEvent pageEvent;

  LoanbPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanbPageState createState() => _LoanbPageState();
}

class _LoanbPageState extends State<LoanbPage> {
  List<String> provinsi;
  String selectedProvinsi;

  @override
  void initState() {
    super.initState();
    provinsi = ['Aceh', 'DKI Jakarta', 'Banten', 'Dan lain lain'];
    // selectedProvinsi = provinsi[0];
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text("Di provinsi mana anda tinggal ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: DropdownButton(
                  value: selectedProvinsi,
                  hint: Text("Provinsi", style: greyTextFont),
                  isExpanded: true,
                  underline: SizedBox(),
                  items: provinsi
                      .map((e) => DropdownMenuItem(
                          value: e,
                          child: Text(
                            e,
                            style: blackTextFont,
                          )))
                      .toList(),
                  onChanged: (item) {
                    setState(() {
                      selectedProvinsi = item;
                    });
                  }),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 8),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoancPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
