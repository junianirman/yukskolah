part of 'pages.dart';

class ElearningDetailPage extends StatelessWidget {
  final PageEvent pageEvent;
  const ElearningDetailPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "E-learning",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Text(
                "Isi Penjelasan Materi",
                style: blackTextFont.copyWith(fontSize: 16),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
