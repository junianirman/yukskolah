part of 'pages.dart';

class ElearningPage extends StatefulWidget {
  ElearningPage({Key key}) : super(key: key);

  @override
  _ElearningPageState createState() => _ElearningPageState();
}

class _ElearningPageState extends State<ElearningPage> {
  TextEditingController elearningController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToMainPage());
        return;
      },
      child: GeneralPage(
        title: "E-learning",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(GoToMainPage());
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.fromLTRB(
                  defaultMargin, defaultMargin, defaultMargin, 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: elearningController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Search"),
              ),
            ),
            Container(
              height: 60,
              width: double.infinity,
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 8),
              child: Column(
                children: [
                  Divider(color: mainColor, thickness: 1),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Icon(Icons.sort, size: 14),
                          Text(
                            "Atur",
                            style: blackTextFont.copyWith(fontSize: 12),
                          )
                        ],
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: defaultMargin),
                        child: Text(
                          '|',
                          textAlign: TextAlign.center,
                          style: blackTextFont.copyWith(
                              fontSize: 22,
                              fontWeight: FontWeight.w600,
                              color: mainColor),
                        ),
                      ),
                      Column(
                        children: [
                          Icon(Icons.filter_alt_outlined, size: 14),
                          Text(
                            "Filter",
                            style: blackTextFont.copyWith(fontSize: 12),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Divider(color: mainColor, thickness: 1)
                ],
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  Wrap(
                    runSpacing: 24,
                    children: dummyElearning
                        .map(
                          (e) => Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: defaultMargin),
                            child: ElearningCard(
                              e,
                              onTap: () {
                                context.read<PageBloc>().add(
                                    GoToElearningMaterialPage(
                                        e, GoToElearningPage()));
                              },
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  SizedBox(height: defaultMargin),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
