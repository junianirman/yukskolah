part of 'pages.dart';

class HomePage extends StatefulWidget {
  final GlobalKey<ScaffoldState> globalKey;

  const HomePage({this.globalKey, Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController findTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  widget.globalKey.currentState.openDrawer();
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Icon(
                      Icons.menu,
                      color: orangeColor,
                      size: 42,
                    ),
                  ),
                ),
              ),
              Container(
                width: 50,
                height: 50,
                child: Center(
                  child: SizedBox(
                    height: 36,
                    child:
                        Image(image: AssetImage("assets/yukskolah_logo.png")),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  context.read<PageBloc>().add(
                        GoToNotificationPage(
                          GoToMainPage(bottomNavBarIndex: 0),
                        ),
                      );
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Icon(Icons.notifications_none_outlined,
                        size: 42, color: orangeColor),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 50,
          width: double.infinity,
          margin: EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 8),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(8)),
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              controller: findTextController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintText: "Find your items"),
            ),
          ),
        ),
        CarouselSlider(
          options: CarouselOptions(
            initialPage: 0,
            autoPlay: true,
            reverse: false,
            enableInfiniteScroll: true,
            viewportFraction: 1.0,
            autoPlayInterval: Duration(seconds: 5),
            autoPlayAnimationDuration: Duration(seconds: 5),
            autoPlayCurve: Curves.fastOutSlowIn,
            scrollDirection: Axis.horizontal,
          ),
          items: dummyEvent
              .map((item) => GestureDetector(
                    onTap: () {
                      context.read<PageBloc>().add(
                            GoToEventDetailPage(
                              item,
                              GoToMainPage(bottomNavBarIndex: 0),
                            ),
                          );
                    },
                    child: Container(
                      margin: EdgeInsets.all(defaultMargin),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        child: Image.asset(item.picturePath,
                            fit: BoxFit.cover, width: 1000),
                      ),
                    ),
                  ))
              .toList(),
        ),
        Stack(
          children: [
            Container(
              width: double.infinity,
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        defaultMargin, defaultMargin, defaultMargin, 8),
                    child: Row(
                      children: [
                        Text(
                          "OurProduct",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Wrap(
                    spacing: 16,
                    runSpacing: 16,
                    alignment: WrapAlignment.spaceBetween,
                    children: [
                      ProductButton(
                        "School",
                        onTap: () {
                          context.read<PageBloc>().add(GoToSchoolPage());
                          return;
                        },
                      ),
                      ProductButton(
                        "Loan",
                        onTap: () {
                          context.read<PageBloc>().add(GoToLoanPage());
                          return;
                        },
                      ),
                      ProductButton(
                        "E-Learning",
                        onTap: () {
                          context.read<PageBloc>().add(GoToElearningPage());
                          return;
                        },
                      ),
                      ProductButton(
                        "Consultation",
                        onTap: () {
                          context.read<PageBloc>().add(GoToConsultationPage());
                          return;
                        },
                      ),
                      ProductButton(
                        "Shopping",
                        onTap: () {
                          context.read<PageBloc>().add(GoToShoppingPage());
                          return;
                        },
                      ),
                      ProductButton(
                        "Promo",
                        onTap: () {
                          context.read<PageBloc>().add(GoToPromoPage());
                          return;
                        },
                      ),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    height: 150,
                    margin: EdgeInsets.fromLTRB(
                        defaultMargin, 32, defaultMargin, defaultMargin),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                          image: AssetImage("assets/travel.jpg"),
                          fit: BoxFit.cover),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: defaultMargin, left: defaultMargin),
                    child: Row(
                      children: [
                        Text(
                          "School",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 250,
                    width: double.infinity,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Row(
                          children: dummySchool
                              .take(4)
                              .map((e) => Padding(
                                    padding: EdgeInsets.only(
                                        left: (e == dummySchool.first)
                                            ? defaultMargin
                                            : 0,
                                        right: defaultMargin),
                                    child: SchoolCard(
                                      e,
                                      onTap: () {
                                        context.read<PageBloc>().add(
                                            GoToSchoolDetailPage(
                                                e, GoToMainPage()));
                                      },
                                    ),
                                  ))
                              .toList(),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: defaultMargin, left: defaultMargin),
                    child: Row(
                      children: [
                        Text(
                          "Consultant",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 175,
                    width: double.infinity,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Row(
                          children: dummyConsultant
                              .map((e) => Padding(
                                    padding: EdgeInsets.only(
                                        left: (e == dummyConsultant.first)
                                            ? defaultMargin
                                            : 0,
                                        right: defaultMargin),
                                    child: ConsultantCard(e),
                                  ))
                              .toList(),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: defaultMargin, left: defaultMargin),
                    child: Row(
                      children: [
                        Text(
                          "Promo",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 150,
                    width: double.infinity,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Row(
                          children: dummyPromo
                              .map(
                                (e) =>
                                    // Padding(
                                    //       padding: EdgeInsets.only(
                                    //           left: (e == dummyPromo.first)
                                    //               ? defaultMargin
                                    //               : 0,
                                    //           right: defaultMargin),
                                    //       child:
                                    PromoCard(e, onTap: () {
                                  context.read<PageBloc>().add(
                                      GoToPromoDetailPage(e, GoToMainPage()));
                                }),
                                // )
                              )
                              .toList(),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 150,
                    margin: EdgeInsets.fromLTRB(
                        defaultMargin, 32, defaultMargin, defaultMargin),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                          image: AssetImage("assets/iklan.jpg"),
                          fit: BoxFit.cover),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: defaultMargin, left: defaultMargin),
                    child: Row(
                      children: [
                        Text(
                          "News",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 150,
                    width: double.infinity,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Row(
                          children: dummyNews
                              .map(
                                (e) =>
                                    // Padding(
                                    //       padding: EdgeInsets.only(
                                    //           left: (e == dummyPromo.first)
                                    //               ? defaultMargin
                                    //               : 0,
                                    //           right: defaultMargin),
                                    //       child:
                                    NewsCard(e, onTap: () {
                                  context.read<PageBloc>().add(
                                      GoToNewsDetailPage(e, GoToMainPage()));
                                }),
                                // )
                              )
                              .toList(),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 100)
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
