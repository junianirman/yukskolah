part of 'pages.dart';

class SchoolRegistrationPage extends StatefulWidget {
  final School school;
  final PageEvent pageEvent;

  SchoolRegistrationPage(this.school, this.pageEvent, {Key key})
      : super(key: key);

  @override
  _SchoolRegistrationPageState createState() => _SchoolRegistrationPageState();
}

class _SchoolRegistrationPageState extends State<SchoolRegistrationPage> {
  TextEditingController registerDateTextController = TextEditingController();
  TextEditingController schoolTextController = TextEditingController();
  TextEditingController nisnTextController = TextEditingController();
  TextEditingController fullNameTextController = TextEditingController();
  TextEditingController placeOfBirthTextController = TextEditingController();
  TextEditingController dateOfBirthTextController = TextEditingController();
  TextEditingController addressTextController = TextEditingController();
  TextEditingController provinsiTextController = TextEditingController();
  TextEditingController cityTextController = TextEditingController();
  TextEditingController kecamatanTextController = TextEditingController();
  TextEditingController kelurahanTextController = TextEditingController();
  TextEditingController rtTextController = TextEditingController();
  TextEditingController rwTextController = TextEditingController();
  TextEditingController phoneTextController = TextEditingController();
  TextEditingController familyCertificateNumberTextController =
      TextEditingController();
  TextEditingController birthCertificateNumberTextController =
      TextEditingController();
  TextEditingController fatherNameTextController = TextEditingController();
  TextEditingController motherNameTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Register",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: registerDateTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Register Date"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: schoolTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: widget.school.name),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: nisnTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "NISN"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: fullNameTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Fullname"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: placeOfBirthTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Place of Birth"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: dateOfBirthTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Date of Birth"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 100,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: addressTextController,
                maxLines: 4,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Address"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: provinsiTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Provinsi"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: cityTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "City"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: kecamatanTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Kecamatan"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: kelurahanTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Kelurahan"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 50,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mainColor)),
                    child: TextField(
                      controller: rtTextController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 8, horizontal: defaultMargin),
                          border: InputBorder.none,
                          hintText: "Rt"),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 50,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mainColor)),
                    child: TextField(
                      controller: rwTextController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 8, horizontal: defaultMargin),
                          border: InputBorder.none,
                          hintText: "Rw"),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: familyCertificateNumberTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Family Certificate Number"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: birthCertificateNumberTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Birth Certificate Number"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: fatherNameTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Fathers Name"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: motherNameTextController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Mothers Name"),
              ),
            ),
            SizedBox(height: defaultMargin),
            Container(
              height: 150,
              // width: MediaQuery.of(context).size.width / 2,
              decoration: BoxDecoration(
                color: Color(0xFFBDBDBD),
                borderRadius: BorderRadius.circular(8),
              ),
              margin: EdgeInsets.symmetric(horizontal: 140),
              child: Icon(Icons.camera_alt, size: 75),
            ),
            SizedBox(height: 32),
            Container(
              height: 45,
              margin: EdgeInsets.symmetric(horizontal: 64),
              child: RaisedButton(
                onPressed: () {},
                elevation: 0,
                color: orangeColor,
                padding: EdgeInsets.symmetric(horizontal: 32),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                child: Text(
                  'Submit',
                  style: blackTextFont.copyWith(fontSize: 18),
                ),
              ),
            ),
            SizedBox(height: defaultMargin),
          ],
        ),
      ),
    );
  }
}
