part of 'pages.dart';

class NewsPage extends StatelessWidget {
  final List<News> news;
  const NewsPage({this.news, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: news.length != 0
            ? ListView.builder(
                itemCount: news.length,
                itemBuilder: (_, index) => Container(
                  margin: EdgeInsets.only(
                      bottom: (index == news.length - 1 ? 75 : 0)),
                  child: NewsCard(
                    news[index],
                    onTap: () {
                      context.read<PageBloc>().add(GoToNewsDetailPage(
                          news[index], GoToMainPage(bottomNavBarIndex: 1)));
                    },
                  ),
                ),
              )
            : Center(
                child: SpinKitFadingCircle(
                  color: mainColor,
                  size: 50,
                ),
              ),
      ),
    );
    // child: Text("News"));
  }
}
