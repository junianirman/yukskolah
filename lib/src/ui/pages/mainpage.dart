part of 'pages.dart';

class MainPage extends StatefulWidget {
  final int bottomNavBarIndex;
  // final bool isExpired;

  MainPage({this.bottomNavBarIndex = 0, Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int bottomNavBarIndex;
  PageController pageController;
  GlobalKey<ScaffoldState> _mainPageScaffoldKey = new GlobalKey();

  @override
  void initState() {
    super.initState();
    bottomNavBarIndex = widget.bottomNavBarIndex;
    pageController = PageController(initialPage: bottomNavBarIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _mainPageScaffoldKey,
      backgroundColor: Color(0xFFF6F7F9),
      drawer: createCustomSideNavBar(),
      body: Stack(
        children: [
          Container(
            color: mainColor,
          ),
          PageView(
            controller: pageController,
            onPageChanged: (index) {
              setState(() {
                bottomNavBarIndex = index;
              });
            },
            children: [
              HomePage(globalKey: _mainPageScaffoldKey),
              NewsPage(news: dummyNews),
              InboxPage(),
              CartPage(),
              AccountPage()
            ],
          ),
          createCustomBottomNavBar(),
        ],
      ),
    );
  }

  Widget createCustomBottomNavBar() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 70,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: BottomNavigationBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedItemColor: mainColor,
          unselectedItemColor: Color(0xFFE5E5E5),
          currentIndex: bottomNavBarIndex,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          selectedLabelStyle:
              GoogleFonts.raleway(fontSize: 13, fontWeight: FontWeight.w600),
          unselectedLabelStyle: blackTextFont.copyWith(
              fontSize: 13, fontWeight: FontWeight.w600, color: Colors.black),
          onTap: (index) {
            setState(() {
              bottomNavBarIndex = index;
              pageController.jumpToPage(index);
            });
          },
          items: [
            BottomNavigationBarItem(
              label: "Home",
              icon: Container(
                height: 20,
                margin: EdgeInsets.only(bottom: 6),
                child: Image.asset((bottomNavBarIndex == 0)
                    ? "assets/ic_home.jpg"
                    : "assets/ic_home_grey.jpeg"),
              ),
            ),
            BottomNavigationBarItem(
              label: "News",
              icon: Container(
                height: 20,
                margin: EdgeInsets.only(bottom: 6),
                child: Image.asset((bottomNavBarIndex == 1)
                    ? "assets/ic_news.png"
                    : "assets/ic_news_grey.jpeg"),
              ),
            ),
            BottomNavigationBarItem(
              label: "Inbox",
              icon: Container(
                height: 20,
                margin: EdgeInsets.only(bottom: 6),
                child: Image.asset((bottomNavBarIndex == 2)
                    ? "assets/ic_inbox.png"
                    : "assets/ic_inbox_grey.png"),
              ),
            ),
            BottomNavigationBarItem(
              label: "Cart",
              icon: Container(
                height: 20,
                margin: EdgeInsets.only(bottom: 6),
                child: Image.asset((bottomNavBarIndex == 3)
                    ? "assets/ic_cart.jpg"
                    : "assets/ic_cart_grey.jpeg"),
              ),
            ),
            BottomNavigationBarItem(
              label: "Account",
              icon: Container(
                height: 20,
                margin: EdgeInsets.only(bottom: 6),
                child: Image.asset((bottomNavBarIndex == 4)
                    ? "assets/ic_account.jpg"
                    : "assets/ic_account_grey.jpeg"),
              ),
            ),
          ],
        ),
      ),
      // ),
    );
  }

  Widget createCustomSideNavBar() {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            title: Text(
              "OurProduct",
              style: blackTextFont.copyWith(
                  fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Divider(thickness: 1, color: Colors.black),
          ),
          ListTile(
            title: Text(
              "School",
              style: blackTextFont,
            ),
            onTap: () {
              context.read<PageBloc>().add(GoToSchoolPage());
            },
          ),
          ListTile(
            title: Text(
              "Loan",
              style: blackTextFont,
            ),
            onTap: () {
              context.read<PageBloc>().add(GoToLoanPage());
            },
          ),
          ListTile(
            title: Text(
              "E-Learning",
              style: blackTextFont,
            ),
            onTap: () {
              context.read<PageBloc>().add(GoToElearningPage());
            },
          ),
          ListTile(
            title: Text(
              "Consultation",
              style: blackTextFont,
            ),
            onTap: () {
              context.read<PageBloc>().add(GoToConsultationPage());
            },
          ),
          ListTile(
            title: Text(
              "Shopping",
              style: blackTextFont,
            ),
            onTap: () {
              context.read<PageBloc>().add(GoToShoppingPage());
            },
          ),
          ListTile(
            title: Text(
              "Promo",
              style: blackTextFont,
            ),
            onTap: () {
              context.read<PageBloc>().add(GoToPromoPage());
            },
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Divider(thickness: 1, color: Colors.black),
          ),
          ListTile(
            title: Text(
              "Account",
              style: blackTextFont,
            ),
            onTap: () {
              context.read<PageBloc>().add(GoToMainPage(bottomNavBarIndex: 4));
            },
          ),
          ListTile(
            title: Text(
              "About",
              style: blackTextFont,
            ),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
