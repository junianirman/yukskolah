part of 'pages.dart';

class PromoDetailPage extends StatelessWidget {
  final Promo promo;
  final PageEvent pageEvent;

  const PromoDetailPage(this.promo, this.pageEvent, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Promo",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: Column(
          children: [
            Container(
              height: 200,
              width: (MediaQuery.of(context).size.width - 32),
              margin: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(promo.picturePath), fit: BoxFit.cover),
              ),
            ),
            Container(
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 8),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Description",
                  style: blackTextFont,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 0, defaultMargin, 24),
              child: Text(
                promo.description,
                style: greyTextFont.copyWith(fontWeight: FontWeight.w400),
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Kode Promo",
                      style: blackTextFont,
                    ),
                    SelectableText(
                      promo.promocode,
                      style: blackTextFont.copyWith(
                          fontWeight: FontWeight.bold, fontSize: 16),
                      onTap: () {
                        Clipboard.setData(ClipboardData(text: promo.promocode));
                      },
                    ),
                    GestureDetector(
                      onTap: () {
                        Clipboard.setData(ClipboardData(text: promo.promocode))
                            .then(
                          (_) => Get.snackbar(
                            "",
                            "",
                            backgroundColor: accentColor2,
                            icon: Icon(MdiIcons.closeCircleOutline,
                                color: Colors.white),
                            titleText: (Text("Copied",
                                style: whiteTextFont.copyWith(
                                    fontWeight: FontWeight.w600))),
                            messageText:
                                Text(promo.promocode, style: whiteTextFont),
                          ),
                        );
                      },
                      child: Icon(Icons.copy),
                    )
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
