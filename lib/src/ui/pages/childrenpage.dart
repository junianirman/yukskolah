part of 'pages.dart';

class ChildrenPage extends StatefulWidget {
  final PageEvent pageEvent;

  const ChildrenPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _ChildrenPageState createState() => _ChildrenPageState();
}

class _ChildrenPageState extends State<ChildrenPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Children",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Container(
          margin: EdgeInsets.all(defaultMargin),
          child: Stack(
            children: [
              Column(
                children: [
                  CustomTabbarChildren(
                    children: dummyChildren,
                    selectedIndex: selectedIndex,
                    onTap: (index) {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  ),
                  Expanded(
                    child: ListView(
                      children: dummyChildren
                          .map(
                            (e) => selectedIndex == dummyChildren.indexOf(e)
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4.2,
                                              child: Text("Name",
                                                  style: blackTextFont)),
                                          Text(": " + e.name,
                                              style: blackTextFont)
                                        ],
                                      ),
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                4.2,
                                            child: Text("Date of Birth",
                                                style: blackTextFont),
                                          ),
                                          Text(": " + e.dateOfBirth,
                                              style: blackTextFont)
                                        ],
                                      ),
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4.2,
                                              child: Text("Class",
                                                  style: blackTextFont)),
                                          Text(": ${e.kelas}",
                                              style: blackTextFont)
                                        ],
                                      ),
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4.2,
                                              child: Text("Email",
                                                  style: blackTextFont)),
                                          Text(": " + e.email,
                                              style: blackTextFont)
                                        ],
                                      ),
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4.2,
                                              child: Text("Password",
                                                  style: blackTextFont)),
                                          Text(": " + e.password,
                                              style: blackTextFont)
                                        ],
                                      ),
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4.2,
                                              child: Text("Phone",
                                                  style: blackTextFont)),
                                          Text(": " + e.phoneNumber,
                                              style: blackTextFont)
                                        ],
                                      ),
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                4.2,
                                            child: Text("School Rating",
                                                style: blackTextFont),
                                          ),
                                          Text(": ", style: blackTextFont),
                                          RatingStars(e.rate)
                                        ],
                                      ),
                                      SizedBox(height: 8),
                                      e.elearning.length == 0
                                          ? Container()
                                          : ChildrenCard("Elearning", e),
                                      e.psycological.length == 0
                                          ? Container()
                                          : ChildrenCard(
                                              "Psycological Test", e),
                                      e.schedule.length == 0
                                          ? Container()
                                          : ChildrenCard("Schedule", e),
                                      e.activities.length == 0
                                          ? Container()
                                          : ChildrenCard("Activity", e),
                                      e.homeWork.length == 0
                                          ? Container()
                                          : ChildrenCard("Home Work", e),
                                      e.book.length == 0
                                          ? Container()
                                          : ChildrenCard("Books", e)
                                    ],
                                  )
                                : Container(),
                          )
                          .toList(),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  child: FloatingActionButton(
                    onPressed: () {
                      // context.read<PageBloc>().add(
                      //     GoToSetupDateTimePage(GoToSetupDatePage(pageEvent)));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: AssetImage('assets/ic_plus.png'),
                        ),
                      ),
                    ),
                    elevation: 0,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
