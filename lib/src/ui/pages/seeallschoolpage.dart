part of 'pages.dart';

class SeeallSchooPage extends StatefulWidget {
  final String title;
  final SchoolType type;
  final List<School> schools;
  final PageEvent pageEvent;

  SeeallSchooPage(this.title, this.type, this.schools, this.pageEvent,
      {Key key})
      : super(key: key);

  @override
  _SeeallSchooPageState createState() => _SeeallSchooPageState();
}

class _SeeallSchooPageState extends State<SeeallSchooPage> {
  TextEditingController seeallController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: widget.title,
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.fromLTRB(
                  defaultMargin, defaultMargin, defaultMargin, 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: seeallController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 8, horizontal: defaultMargin),
                    border: InputBorder.none,
                    hintText: "Search"),
              ),
            ),
            Container(
              height: 60,
              width: double.infinity,
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 8),
              child: Column(
                children: [
                  Divider(color: mainColor, thickness: 1),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Icon(Icons.sort, size: 14),
                          Text(
                            "Atur",
                            style: blackTextFont.copyWith(fontSize: 12),
                          )
                        ],
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: defaultMargin),
                        child: Text(
                          '|',
                          textAlign: TextAlign.center,
                          style: blackTextFont.copyWith(
                              fontSize: 22,
                              fontWeight: FontWeight.w600,
                              color: mainColor),
                        ),
                      ),
                      Column(
                        children: [
                          Icon(Icons.filter_alt_outlined, size: 14),
                          Text(
                            "Filter",
                            style: blackTextFont.copyWith(fontSize: 12),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Divider(color: mainColor, thickness: 1)
                ],
              ),
            ),
            Expanded(
              child: ListView(children: [
                Wrap(
                  runSpacing: 24,
                  children: getListFromType(widget.type)
                      .map(
                        (e) => Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: defaultMargin),
                          child: SchoolCard(
                            e,
                            onTap: () {
                              context.read<PageBloc>().add(
                                    GoToSchoolDetailPage(
                                      e,
                                      GoToSeeallSchoolPage(
                                          widget.title,
                                          widget.type,
                                          getListFromType(widget.type),
                                          widget.pageEvent),
                                    ),
                                  );
                            },
                          ),
                        ),
                      )
                      .toList(),
                ),
                SizedBox(height: defaultMargin),
              ]),
            ),
          ],
        ),
      ),
    );
  }

  List<School> getListFromType(SchoolType type) {
    switch (type) {
      case SchoolType.kindergarten:
        return widget.schools
            .where((element) => element.types == SchoolType.kindergarten)
            .toList();
        break;
      case SchoolType.elementary_school:
        return widget.schools
            .where((element) => element.types == SchoolType.elementary_school)
            .toList();
        break;
      case SchoolType.junior_high_school:
        return widget.schools
            .where((element) => element.types == SchoolType.junior_high_school)
            .toList();
        break;
      case SchoolType.senior_high_school:
        return widget.schools
            .where((element) => element.types == SchoolType.senior_high_school)
            .toList();
        break;
      default:
        return [];
    }
  }
}
