part of 'pages.dart';

class AccountDetailPage extends StatelessWidget {
  final User user;
  final PageEvent pageEvent;

  const AccountDetailPage(this.user, this.pageEvent, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Profile",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  width: 150,
                  height: 150,
                  margin: EdgeInsets.symmetric(vertical: 32),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(user.picturePath), fit: BoxFit.cover),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: defaultMargin, vertical: 4),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Name",
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      user.name,
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.w400),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
                SizedBox(height: 24),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: defaultMargin, vertical: 4),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Date of Birth",
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      user.dateOfBirth,
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.w400),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
                SizedBox(height: 24),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: defaultMargin, vertical: 4),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Address",
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      user.address,
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.w400),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      user.phoneNumber,
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.w400),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
                SizedBox(height: 24),
                GestureDetector(
                  onTap: () {
                    context
                        .read<PageBloc>()
                        .add(GoToChangePasswordPage(user, pageEvent));
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: defaultMargin, vertical: 4),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Change Password ?",
                        style:
                            blackTextFont.copyWith(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
