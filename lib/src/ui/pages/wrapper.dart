part of 'pages.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!(prevPageEvent is GoToSplashPage)) {
      prevPageEvent = GoToSplashPage();
      context.watch<PageBloc>().add(prevPageEvent);
    }

    return BlocBuilder<PageBloc, PageState>(
      builder: (_, pageState) => (pageState is OnSplashPage)
          ? SplashPage()
          : (pageState is OnMainPage)
              ? MainPage(bottomNavBarIndex: pageState.bottomNavBarIndex)
              : (pageState is OnNotificationPage)
                  ? NotificationPage(pageState.pageEvent)
                  : (pageState is OnSchoolPage)
                      ? SchoolPage()
                      : (pageState is OnLoanPage)
                          ? LoanPage()
                          : (pageState is OnElearningPage)
                              ? ElearningPage()
                              : (pageState is OnConsultationPage)
                                  ? ConsultationPage()
                                  : (pageState is OnShoppingPage)
                                      ? ShoppingPage()
                                      : (pageState is OnPromoPage)
                                          ? PromoPage()
                                          : (pageState is OnPromoDetailPage)
                                              ? PromoDetailPage(pageState.promo,
                                                  pageState.pageEvent)
                                              : (pageState is OnNewsDetailPage)
                                                  ? NewsDetailPage(
                                                      pageState.news,
                                                      pageState.pageEvent)
                                                  : (pageState
                                                          is OnEventDetailPage)
                                                      ? EventDetailPage(
                                                          pageState.event,
                                                          pageState.pageEvent)
                                                      : (pageState
                                                              is OnChatPage)
                                                          ? ChatPage(
                                                              pageState.inbox,
                                                              pageState
                                                                  .pageEvent)
                                                          : (pageState
                                                                  is OnSeeallSchoolPage)
                                                              ? SeeallSchooPage(
                                                                  pageState
                                                                      .title,
                                                                  pageState
                                                                      .type,
                                                                  pageState
                                                                      .schools,
                                                                  pageState
                                                                      .pageEvent)
                                                              : (pageState
                                                                      is OnSchoolDetailPage)
                                                                  ? SchoolDetailPage(
                                                                      pageState
                                                                          .school,
                                                                      pageState
                                                                          .pageEvent)
                                                                  : (pageState
                                                                          is OnSchoolInformationPage)
                                                                      ? SchoolInformationPage(
                                                                          pageState
                                                                              .school,
                                                                          pageState
                                                                              .pageEvent)
                                                                      : (pageState
                                                                              is OnSchoolRegistrationPage)
                                                                          ? SchoolRegistrationPage(
                                                                              pageState.school,
                                                                              pageState.pageEvent)
                                                                          : (pageState is OnAccountDetailPage)
                                                                              ? AccountDetailPage(pageState.user, pageState.pageEvent)
                                                                              : (pageState is OnChildrenPage)
                                                                                  ? ChildrenPage(pageState.pageEvent)
                                                                                  : (pageState is OnElearningMaterialPage)
                                                                                      ? ElearningMaterialPage(pageState.elearning, pageState.pageEvent)
                                                                                      : (pageState is OnElearningDetailPage)
                                                                                          ? ElearningDetailPage(pageState.pageEvent)
                                                                                          : (pageState is OnShoppingDetailPage)
                                                                                              ? ShoppingDetailPage(pageState.product, pageState.pageEvent)
                                                                                              : (pageState is OnLoanaPage)
                                                                                                  ? LoanaPage(pageState.pageEvent)
                                                                                                  : (pageState is OnLoanbPage)
                                                                                                      ? LoanbPage(pageState.pageEvent)
                                                                                                      : (pageState is OnLoancPage)
                                                                                                          ? LoancPage(pageState.pageEvent)
                                                                                                          : (pageState is OnLoandPage)
                                                                                                              ? LoandPage(pageState.pageEvent)
                                                                                                              : (pageState is OnLoanePage)
                                                                                                                  ? LoanePage(pageState.pageEvent)
                                                                                                                  : (pageState is OnLoanfPage)
                                                                                                                      ? LoanfPage(pageState.pageEvent)
                                                                                                                      : (pageState is OnLoangPage)
                                                                                                                          ? LoangPage(pageState.pageEvent)
                                                                                                                          : (pageState is OnLoanhPage)
                                                                                                                              ? LoanhPage(pageState.pageEvent)
                                                                                                                              : (pageState is OnLoaniPage)
                                                                                                                                  ? LoaniPage(pageState.pageEvent)
                                                                                                                                  : (pageState is OnLoanjPage)
                                                                                                                                      ? LoanjPage(pageState.pageEvent)
                                                                                                                                      : (pageState is OnLoankPage)
                                                                                                                                          ? LoankPage(pageState.pageEvent)
                                                                                                                                          : (pageState is OnLoanlPage)
                                                                                                                                              ? LoanlPage(pageState.pageEvent)
                                                                                                                                              : (pageState is OnLoanResultPage)
                                                                                                                                                  ? LoanResultPage(pageState.pageEvent)
                                                                                                                                                  : (pageState is OnChangePasswordPage)
                                                                                                                                                      ? ChangePasswordPage(pageState.user, pageState.pageEvent)
                                                                                                                                                      : (pageState is OnWishlistPage)
                                                                                                                                                          ? WishlistPage(pageState.pageEvent)
                                                                                                                                                          : (pageState is OnTransactionPage)
                                                                                                                                                              ? TransactionPage(pageState.pageEvent)
                                                                                                                                                              : (pageState is OnTransactionHistoryPage)
                                                                                                                                                                  ? TransactionHistoryPage(pageState.pageEvent)
                                                                                                                                                                  : Container(),
    );
  }
}
