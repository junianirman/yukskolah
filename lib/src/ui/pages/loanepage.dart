part of 'pages.dart';

class LoanePage extends StatefulWidget {
  final PageEvent pageEvent;

  LoanePage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _LoanePageState createState() => _LoanePageState();
}

class _LoanePageState extends State<LoanePage> {
  TextEditingController penghasilanController = TextEditingController();
  int selectedAmount = 0;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Loan",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text("Berapa penghasilan anda perbulan ?",
                  style: blackTextFont.copyWith(fontSize: 16)),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: mainColor)),
              child: TextField(
                controller: penghasilanController,
                keyboardType: TextInputType.number,
                onChanged: (text) {
                  String temp = '';
                  for (int i = 0; i <= text.length; i++) {
                    temp += text.isDigit(i) ? text[i] : '';
                  }
                  setState(() {
                    selectedAmount = int.tryParse(temp) ?? 0;
                  });
                  penghasilanController.text = NumberFormat.currency(
                          locale: 'id_ID', symbol: 'IDR ', decimalDigits: 0)
                      .format(selectedAmount);
                  penghasilanController.selection = TextSelection.fromPosition(
                      TextPosition(offset: penghasilanController.text.length));
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              margin: EdgeInsets.only(top: 8),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  context.read<PageBloc>().add(GoToLoanfPage(widget.pageEvent));
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: orangeColor,
                child: Text(
                  'Next',
                  style: blackTextFont.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
