part of 'models.dart';

enum VarianType { varian, deskripsi, seller, pengiriman, promo, ulasan }

class Product extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final int price;
  final List<Varian> varian;
  final List<int> size;
  final Seller seller;
  final int discount;
  final String promoCode;
  final List<Ulasan> ulasan;

  Product(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.price,
      this.varian,
      this.size,
      this.seller,
      this.discount,
      this.promoCode,
      this.ulasan});

  @override
  List<Object> get props => [
        id,
        picturePath,
        name,
        description,
        seller,
        price,
        varian,
        size,
        seller,
        discount,
        promoCode,
        ulasan
      ];
}

List<Product> dummyProduct = [
  Product(
      id: 1,
      picturePath: "assets/shoes_1.jpg",
      name: "Sepatu Baru",
      description: "",
      price: 120000,
      varian: [
        Varian(picturePath: "assets/shoes_1.jpg"),
        Varian(picturePath: "assets/shoes_3.jpg")
      ],
      size: [41, 42],
      seller: Seller(
          name: "Toko Sepatu Serba Ada",
          rate: 4,
          address: "Jakarta Selatan",
          picturePath: "assets/shop.jpg"),
      discount: 5,
      promoCode: "ABCDEFGH",
      ulasan: [
        Ulasan(
            user: dummyUser,
            rate: 4,
            ulasan: "Barang bagus sekali, seperti original",
            picturePath: "assets/shoes_1.jpg")
      ]),
  Product(
      id: 2,
      picturePath: "assets/shoes_2.jpg",
      name: "Model ABCD",
      description: "",
      price: 120000,
      size: [41, 42],
      varian: [
        Varian(picturePath: "assets/shoes_1.jpg"),
        Varian(picturePath: "assets/shoes_3.jpg")
      ],
      seller: Seller(
          name: "Toko Sepatu Serba Ada",
          rate: 4,
          address: "Jakarta Selatan",
          picturePath: "assets/shop.jpg"),
      discount: 5,
      promoCode: "ABCDEFGH",
      ulasan: [
        Ulasan(
            user: dummyUser,
            rate: 4,
            ulasan: "Barang bagus sekali, seperti original",
            picturePath: "assets/shoes_1.jpg")
      ]),
];
