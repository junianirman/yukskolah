part of 'models.dart';

class Promo extends Equatable {
  final int id;
  final String name;
  final String promocode;
  final String description;
  final String picturePath;

  Promo(
      {@required this.id,
      @required this.name,
      @required this.promocode,
      @required this.description,
      @required this.picturePath});

  @override
  List<Object> get props => [id, name, promocode, description, picturePath];
}

List<Promo> dummyPromo = [
  Promo(
      id: 1,
      name: "Paling Hoki",
      promocode: "PALINGHOKI",
      description:
          "Promo ini berlaku hingga 28 februari 2021\nMasukan kode voucher \b\"PALINGHOKI\"\b untuk mendapatkan potongan Rp 1.000.000,-\nPromo berlaku untuk minimal sewa 3 bulan\nPromo berlaku untuk unit apartemen tertentu\nPromo hanya berlaku di aplikasi travelio\nSebelum tamu check-in kamar akan kami semprot disinfectant terlebih dahulu agar tetap steril\nSetiap tamu yang check-in akan diberikan hand sanitizer yang dapat digunakan oleh tamu\nPromo ini berlaku untuk check in maksimal 30 April 2021",
      picturePath: "assets/promo_1.jpg"),
  Promo(
      id: 2,
      name: "Sebut",
      promocode: "SEBUT",
      description: "",
      picturePath: "assets/promo_2.jpg"),
  Promo(
      id: 3,
      name: "Promo Heboh",
      promocode: "HEBOH",
      description: "",
      picturePath: "assets/promo_3.jpg")
];
