part of 'models.dart';

enum SchoolType {
  kindergarten,
  elementary_school,
  junior_high_school,
  senior_high_school
}

class School extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String information;
  final String accreditation;
  final int minGrade;
  final int maxGrade;
  final int price;
  final double rate;
  final SchoolType types;

  School({
    this.id,
    this.picturePath,
    this.name,
    this.description,
    this.information,
    this.accreditation,
    this.minGrade,
    this.maxGrade,
    this.price,
    this.rate,
    this.types,
  });

  @override
  List<Object> get props => [
        id,
        picturePath,
        name,
        description,
        information,
        accreditation,
        minGrade,
        maxGrade,
        price,
        rate
      ];
}

List<School> dummySchool = [
  School(
      id: 1,
      picturePath: "assets/kindergarten_1.jpg",
      name: "Kinderland Preschool",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.kindergarten),
  School(
      id: 2,
      picturePath: "assets/kindergarten_2.jpg",
      name: "Bambino Preschool",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.kindergarten),
  School(
      id: 3,
      picturePath: "assets/kindergarten_3.jpg",
      name: "ABCD Preschool",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.kindergarten),
  School(
      id: 4,
      picturePath: "assets/kindergarten_4.jpg",
      name: "Oreo Preschool",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.kindergarten),
  School(
      id: 5,
      picturePath: "assets/elementary_1.jpg",
      name: "Sekolah Cita Buana",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.elementary_school),
  School(
      id: 6,
      picturePath: "assets/elementary_2.jpg",
      name: "SDN Pancoran 01 Pagi",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.elementary_school),
  School(
      id: 5,
      picturePath: "assets/junior_1.jpg",
      name: "SMP Negeri 115 Jakarta",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.junior_high_school),
  School(
      id: 6,
      picturePath: "assets/junior_2.jpg",
      name: "SMP Negeri 41 Jakarta",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.junior_high_school),
  School(
      id: 5,
      picturePath: "assets/senior_1.jpg",
      name: "SMA Tarakanita",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.senior_high_school),
  School(
      id: 6,
      picturePath: "assets/senior_2.jpg",
      name: "SMA Dua Mei",
      description:
          "is an enriched preschool, engaging children in a joyful and dynamic environment. Montclare inspires lifelong learners by cultivating collaboration, creativity and independent thingking",
      information:
          "Before 1960, the education of young children was primarily regarded as the responsibility of families within the home. As of 2004, most young children in the United States spend some portion of their days apart from their parents. Most attend some sort of center-based program prior to kindergarten. In 2001, 52 percent of three- and four-year-olds were in a nursery school or preschool program. The enrollment rate for four-year-olds in 2001 was nearly the same as the enrollment rate for five-year-olds in 1970. There are several factors influencing this dramatic change, including a rise in the numbers of mothers working outside the home, a decline in the size of families (leading more parents to turn to preschools as a social outlet for their children), and a growing desire to give children a head start academically. The higher the income and educational level of the parents, the more likely it is that a child will attend preschool. This correlation remains true in spite of increasing governmental support for programs targeting children in low-income households.",
      accreditation: "A",
      minGrade: 90,
      maxGrade: 98,
      price: 150000,
      rate: 5,
      types: SchoolType.senior_high_school)
];
