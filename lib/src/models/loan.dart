part of 'models.dart';

class Loan extends Equatable {
  final int id;
  final String name;
  final String title;
  final double bunga;
  final int cicilan;
  final int total;
  final String picturePath;

  Loan(
      {this.id,
      this.name,
      this.title,
      this.bunga,
      this.cicilan,
      this.total,
      this.picturePath});

  @override
  List<Object> get props => [id, name, title, bunga, cicilan, picturePath];
}

List<Loan> dummyLoan = [
  Loan(
      id: 1,
      name: "Indodana",
      title: "Indodana tanpa kartu kredit",
      bunga: 8,
      cicilan: 1633333,
      total: 0,
      picturePath: "assets/loan_1.jpg"),
  Loan(
      id: 2,
      name: "CIMB Niaga",
      title: "CIMB Niaga Xtra Dana",
      bunga: 1.79,
      cicilan: 1012333,
      total: 0,
      picturePath: "assets/loan_2.png"),
];
