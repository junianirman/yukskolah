part of 'models.dart';

class Event extends Equatable {
  final int id;
  final String title;
  final String place;
  final int price;
  final String picturePath;

  Event(
      {@required this.id,
      @required this.title,
      @required this.place,
      @required this.price,
      @required this.picturePath});

  @override
  List<Object> get props => [id, title, place, price, picturePath];
}

List<Event> dummyEvent = [
  Event(
      id: 1,
      title: "Perlombaan 17 Agustus",
      place: "SMA 8 Jakarta",
      price: 10000,
      picturePath: "assets/junior_1.jpg"),
  Event(
      id: 2,
      title: "Perlombaan 17 Agustus",
      place: "SMA 8 Jakarta",
      price: 10000,
      picturePath: "assets/junior_2.jpg"),
  Event(
      id: 3,
      title: "Perlombaan 17 Agustus",
      place: "SMA 8 Jakarta",
      price: 10000,
      picturePath: "assets/elementary_1.jpg"),
  Event(
      id: 4,
      title: "Perlombaan 17 Agustus",
      place: "SMA 8 Jakarta",
      price: 10000,
      picturePath: "assets/event_4.jpg"),
];
