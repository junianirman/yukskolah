part of 'models.dart';

class SetupDateTime extends Equatable {
  final int id;
  final DateTime startTime;
  final DateTime endTime;
  final DateTime day;

  SetupDateTime({this.id, this.startTime, this.endTime, this.day});

  @override
  List<Object> get props => [id, startTime, endTime, day];
}
