part of 'models.dart';

class Elearning extends Equatable {
  final int id;
  final String title;
  final String major;
  final int kelas;
  final String curriculum;
  final String lecturer;
  final double rate;
  final int price;
  final String picturePath;
  final String elearningDate;
  final List<ElearningMaterial> material;
  // final List<String> subPicturePath;

  Elearning({
    this.id,
    this.title,
    this.major,
    this.kelas,
    this.curriculum,
    this.lecturer,
    this.rate,
    this.price,
    this.picturePath,
    this.elearningDate,
    this.material,
  });

  @override
  List<Object> get props => [
        id,
        title,
        major,
        kelas,
        curriculum,
        lecturer,
        rate,
        price,
        picturePath,
        elearningDate,
        material,
      ];
}

List<Elearning> dummyElearning = [
  Elearning(
    id: 1,
    title: "IPA",
    major: "IPA",
    kelas: 9,
    curriculum: "K13",
    lecturer: "Ruang Guru",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_1.png",
    elearningDate: "10-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  ),
  Elearning(
    id: 2,
    title: "IPS",
    major: "IPS",
    kelas: 12,
    curriculum: "K13",
    lecturer: "Ruang Guru",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_2.jpg",
    elearningDate: "11-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  ),
  Elearning(
    id: 3,
    title: "IPA",
    major: "IPA",
    kelas: 9,
    curriculum: "K13",
    lecturer: "Udemy",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_3.jpg",
    elearningDate: "12-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  ),
  Elearning(
    id: 4,
    title: "IPS",
    major: "IPS",
    kelas: 9,
    curriculum: "K13",
    lecturer: "Udemy",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_4.jpg",
    elearningDate: "13-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  ),
  Elearning(
    id: 5,
    title: "Matematika",
    major: "IPA",
    kelas: 9,
    curriculum: "K13",
    lecturer: "Ruang Guru",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_5.jpg",
    elearningDate: "14-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  ),
  Elearning(
    id: 6,
    title: "Matematika",
    major: "IPA",
    kelas: 9,
    curriculum: "K13",
    lecturer: "Udemy",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_6.jpg",
    elearningDate: "15-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  ),
  Elearning(
    id: 7,
    title: "Geografi",
    major: "IPS",
    kelas: 9,
    curriculum: "K13",
    lecturer: "Ruang Guru",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_7.jpg",
    elearningDate: "16-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  ),
  Elearning(
    id: 8,
    title: "Bahasa Indonesia",
    major: "IPA",
    kelas: 9,
    curriculum: "K13",
    lecturer: "Udemy",
    rate: 5,
    price: 150000,
    picturePath: "assets/book_8.jpg",
    elearningDate: "17-10-2020",
    material: [
      ElearningMaterial(
          title: "Sifat Kolegatif Larutan",
          description: "",
          picturePath: "assets/subelearning_1.png"),
      ElearningMaterial(
          title: "Reaksi Redoks",
          description: "",
          picturePath: "assets/subelearning_2.png"),
      ElearningMaterial(
          title: "Video Penjelasan Rumus",
          description: "",
          picturePath: "assets/subelearning_3.jpg"),
    ],
  )
];
