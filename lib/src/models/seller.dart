part of 'models.dart';

class Seller extends Equatable {
  final String name;
  final double rate;
  final String address;
  final String picturePath;

  Seller({this.name, this.rate, this.address, this.picturePath});

  @override
  List<Object> get props => [name, rate, address, picturePath];
}
