part of 'models.dart';

class Activities extends Equatable {
  final int id;
  final String name;
  final String picturePath;
  final String activitiesDate;

  Activities({this.id, this.name, this.picturePath, this.activitiesDate});

  @override
  List<Object> get props => [id, name, picturePath];
}

List<Activities> dummyActivities = [
  Activities(
      id: 1,
      name: "Karate",
      picturePath: "assets/activities_1.jpg",
      activitiesDate: ""),
  Activities(
      id: 2,
      name: "Pramuka",
      picturePath: "assets/activities_2.jpg",
      activitiesDate: ""),
  Activities(
      id: 3,
      name: "PMR",
      picturePath: "assets/activities_3.jpg",
      activitiesDate: ""),
  Activities(
      id: 4, name: "Free", picturePath: "", activitiesDate: "17-08-2020"),
  Activities(id: 5, name: "PMI", picturePath: "", activitiesDate: "18-08-2020")
];
