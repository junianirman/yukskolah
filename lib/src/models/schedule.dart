part of 'models.dart';

enum ScheduleStatus { schedule, reschedule, children }

class Schedule extends Equatable {
  final int id;
  final List<String> name;
  final String scheduleDate;
  final Consultant consultant;
  final ScheduleStatus status;

  Schedule(
      {this.id, this.name, this.scheduleDate, this.consultant, this.status});

  @override
  List<Object> get props => [id, consultant, status];
}

List<Schedule> dummySchedule = [
  Schedule(
      id: 1,
      name: [],
      scheduleDate: "",
      consultant: dummyConsultant[0],
      status: ScheduleStatus.reschedule),
  Schedule(
      id: 2,
      name: [],
      scheduleDate: "",
      consultant: Consultant(
        id: 2,
        name: "Dr. Stefani Hans",
        profession: "Psikolog",
        age: 20,
        rate: 4,
        price: 200000,
        picturePath: "assets/user_pic.jpg",
        setupDateTime: [
          SetupDateTime(
            id: 1,
            startTime: DateTime.now(),
            endTime: DateTime.now().add(Duration(minutes: 30)),
            day: DateTime.now(),
          ),
        ],
      ),
      status: ScheduleStatus.schedule),
  Schedule(
      id: 3,
      name: ["Ceremony"],
      scheduleDate: "10-10-2020",
      status: ScheduleStatus.children),
  Schedule(
      id: 4,
      name: ["Math", "Indonesia"],
      scheduleDate: "10-10-2020",
      status: ScheduleStatus.children),
];
