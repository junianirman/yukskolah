part of 'models.dart';

class Facilities extends Equatable {
  final int id;
  final String name;
  final String picturePath;

  Facilities(
      {@required this.id, @required this.name, @required this.picturePath});

  @override
  List<Object> get props => [id, name, picturePath];
}

List<Facilities> dummyFacilities = [
  Facilities(
      id: 1, name: "Lapangan Basket", picturePath: "assets/facilities_1.jpg"),
  Facilities(
      id: 2, name: "Perpustakaan", picturePath: "assets/facilities_2.jpg"),
  Facilities(
      id: 3, name: "Lab Komputer", picturePath: "assets/facilities_3.jpg"),
  Facilities(id: 4, name: "Lab Fisika", picturePath: "assets/facilities_4.jpg")
];
