part of 'models.dart';

class ElearningMaterial extends Equatable {
  final String title;
  final String description;
  final String picturePath;

  ElearningMaterial({this.title, this.description, this.picturePath});

  @override
  List<Object> get props => [title, description, picturePath];
}
