part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String dateOfBirth;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;
  final int balance;

  User(
      {this.id,
      this.name,
      this.dateOfBirth,
      this.email,
      this.address,
      this.houseNumber,
      this.phoneNumber,
      this.city,
      this.picturePath,
      this.balance});

  User copyWith(
          {int id,
          String name,
          String dateOfBirth,
          String email,
          String address,
          String houseNumber,
          String phoneNumber,
          String city,
          String picturePath,
          int balance}) =>
      User(
          id: id ?? this.id,
          name: name ?? this.name,
          dateOfBirth: dateOfBirth ?? this.dateOfBirth,
          email: email ?? this.email,
          address: address ?? this.address,
          houseNumber: houseNumber ?? this.houseNumber,
          phoneNumber: phoneNumber ?? this.phoneNumber,
          city: city ?? this.city,
          picturePath: picturePath ?? this.picturePath,
          balance: balance ?? this.balance);

  @override
  List<Object> get props => [
        id,
        name,
        dateOfBirth,
        email,
        address,
        houseNumber,
        phoneNumber,
        city,
        picturePath,
        balance
      ];
}

User dummyUser = User(
    id: 1,
    name: 'Mahroza Pradana',
    dateOfBirth: '08-01-1990',
    address:
        'Jl. Cilandak 47, No. 889, Rt007/008, Cilandak, Pasar Minggu, \nJakarta Selatan, DKI Jakarta, 12545',
    city: 'Jakarta',
    houseNumber: '889',
    phoneNumber: '081223456789',
    email: 'mahroza.pradana1980@gmail.com',
    picturePath: 'assets/user_pic.jpg',
    balance: 512000);
