part of 'models.dart';

class Ulasan extends Equatable {
  final User user;
  final double rate;
  final String ulasan;
  final String picturePath;

  Ulasan({this.user, this.rate, this.ulasan, this.picturePath});

  @override
  List<Object> get props => [user, rate, ulasan, picturePath];
}
