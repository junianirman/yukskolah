part of 'models.dart';

class Consultant extends Equatable {
  final int id;
  final String name;
  final String profession;
  final int age;
  final int price;
  final double rate;
  final String picturePath;
  final List<SetupDateTime> setupDateTime;

  Consultant({
    this.id,
    this.name,
    this.profession,
    this.age,
    this.price,
    this.rate,
    this.picturePath,
    this.setupDateTime,
  });

  @override
  List<Object> get props => [
        id,
        name,
        profession,
        age,
        price,
        rate,
        picturePath,
        setupDateTime,
      ];
}

List<Consultant> dummyConsultant = [
  Consultant(
    id: 1,
    name: "C. Vaile Wright, PhD",
    profession: "Psikolog",
    age: 20,
    rate: 4,
    price: 200000,
    picturePath: "assets/user_pic.jpg",
    setupDateTime: [
      SetupDateTime(
        id: 1,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
      SetupDateTime(
        id: 2,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
      SetupDateTime(
        id: 3,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
      SetupDateTime(
        id: 4,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
      SetupDateTime(
        id: 5,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
      SetupDateTime(
        id: 6,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
    ],
  ),
  Consultant(
    id: 2,
    name: "Adinda, PhD",
    profession: "Psikolog",
    age: 10,
    rate: 4,
    price: 200000,
    picturePath: "assets/user_pic.jpg",
    setupDateTime: [
      SetupDateTime(
        id: 1,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
    ],
  ),
  Consultant(
    id: 3,
    name: "C. Vaile Wright, PhD",
    profession: "Psikolog",
    age: 20,
    rate: 4,
    price: 200000,
    picturePath: "assets/user_pic.jpg",
    setupDateTime: [
      SetupDateTime(
        id: 1,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
    ],
  ),
  Consultant(
    id: 4,
    name: "C. Vaile Wright, PhD",
    profession: "Psikolog",
    age: 20,
    rate: 4,
    price: 200000,
    picturePath: "assets/user_pic.jpg",
    setupDateTime: [
      SetupDateTime(
        id: 1,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
    ],
  ),
  Consultant(
    id: 5,
    name: "C. Vaile Wright, PhD",
    profession: "Psikolog",
    age: 20,
    rate: 4,
    price: 200000,
    picturePath: "assets/user_pic.jpg",
    setupDateTime: [
      SetupDateTime(
        id: 1,
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(minutes: 30)),
        day: DateTime.now(),
      ),
    ],
  )
];
