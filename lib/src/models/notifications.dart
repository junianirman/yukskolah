part of 'models.dart';

class Notifications extends Equatable {
  final int id;
  final String status;
  final String picturePath;
  final String notificationDate;

  Notifications(
      {this.id, this.status, this.picturePath, this.notificationDate});

  @override
  List<Object> get props => [id, status, picturePath];
}

List<Notifications> dummyNotification = [
  Notifications(
      id: 1,
      status: "Pesanan anda sedang di proses pengiriman",
      picturePath: "assets/shoes_1.jpg",
      notificationDate: "2020-10-05"),
  Notifications(
      id: 2,
      status: "Pesanan anda sedang di proses oleh penjual",
      picturePath: "assets/shoes_1.jpg",
      notificationDate: "2020-10-05"),
  Notifications(
      id: 3,
      status: "Pembayaran anda berhasil no tagihan anda ABCD1234567890",
      picturePath: "assets/shoes_1.jpg",
      notificationDate: "2020-10-05"),
];
