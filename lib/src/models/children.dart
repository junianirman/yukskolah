part of 'models.dart';

class Children extends Equatable {
  final int id;
  final String name;
  final String dateOfBirth;
  final int kelas;
  final String school;
  final String email;
  final String password;
  final String phoneNumber;
  final double rate;
  final String picturePath;
  final List<Elearning> elearning;
  final List<Psycological> psycological;
  final List<Schedule> schedule;
  final List<Activities> activities;
  final List<HomeWork> homeWork;
  final List<Book> book;

  Children({
    this.id,
    this.name,
    this.dateOfBirth,
    this.kelas,
    this.school,
    this.email,
    this.password,
    this.phoneNumber,
    this.rate,
    this.picturePath,
    this.elearning,
    this.psycological,
    this.schedule,
    this.activities,
    this.homeWork,
    this.book,
  });

  @override
  List<Object> get props => [
        id,
        name,
        dateOfBirth,
        kelas,
        school,
        email,
        password,
        phoneNumber,
        rate,
        picturePath,
        elearning,
        psycological,
        schedule,
        activities,
        homeWork,
        book
      ];
}

List<Children> dummyChildren = [
  Children(
    id: 1,
    name: "Ananda Anak Pratama",
    dateOfBirth: "19-04-2005",
    kelas: 9,
    school: "SMA Negeri 8 Jakarta",
    email: "ananda@gmail.com",
    password: "*********",
    phoneNumber: "081278903456",
    rate: 5,
    picturePath: "assets/user_pic.jpg",
    elearning: [dummyElearning[0]],
    psycological: [dummyPsycological[0]],
    schedule: [dummySchedule[2], dummySchedule[3]],
    activities: [dummyActivities[3], dummyActivities[4]],
    homeWork: dummyHomeWork,
    book: dummyBook,
  ),
  Children(
    id: 2,
    name: "Adinda Adik Pertama",
    dateOfBirth: "19-08-2008",
    kelas: 8,
    school: "SMP 7 Jakarta",
    email: "adinda@gmail.com",
    password: "*********",
    phoneNumber: "081278903457",
    rate: 5,
    picturePath: "assets/user_pic.jpg",
    elearning: [],
    psycological: [],
    schedule: [dummySchedule[2], dummySchedule[3]],
    activities: [dummyActivities[3], dummyActivities[4]],
    homeWork: dummyHomeWork,
    book: dummyBook,
  )
];

class HomeWork extends Equatable {
  final int id;
  final String name;
  final String homeWorkDate;

  HomeWork({this.id, this.name, this.homeWorkDate});

  @override
  List<Object> get props => [id, name, homeWorkDate];
}

List<HomeWork> dummyHomeWork = [
  HomeWork(id: 1, name: "math linier hal.20", homeWorkDate: "17-19-2020"),
  HomeWork(id: 2, name: "Biologi", homeWorkDate: "10-08-2020"),
];

class Book extends Equatable {
  final int id;
  final String name;
  final String bookDate;

  Book({this.id, this.name, this.bookDate});

  @override
  List<Object> get props => [id, name, bookDate];
}

List<Book> dummyBook = [
  Book(id: 1, name: "Technology", bookDate: "10-10-2020"),
  Book(id: 2, name: "Biology", bookDate: "10-10-2020"),
];
