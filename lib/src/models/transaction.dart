part of 'models.dart';

enum TransactionType {
  school,
  loan,
  product,
  elearning,
  consultation,
  psycological
}

class Transaction extends Equatable {
  final int id;
  final int quantity;
  final int shippingCost;
  final int total;
  final String status;
  final String orderNumber;
  final String deliveryService;
  final String invoice;
  final String receipt;
  final String orderDate;
  final String payment;
  final Product product;
  final School school;
  final Loan loan;
  final Elearning elearning;
  final Consultant consultant;
  final Psycological psycological;
  final TransactionType transactionType;

  Transaction({
    this.id,
    this.quantity,
    this.shippingCost,
    this.total,
    this.status,
    this.orderNumber,
    this.deliveryService,
    this.invoice,
    this.receipt,
    this.orderDate,
    this.payment,
    this.product,
    this.school,
    this.loan,
    this.elearning,
    this.consultant,
    this.psycological,
    this.transactionType,
  });

  @override
  List<Object> get props => [
        id,
        quantity,
        shippingCost,
        total,
        status,
        orderNumber,
        deliveryService,
        invoice,
        receipt,
        orderDate,
        payment,
        product,
        school,
        loan,
        elearning,
        consultant,
        psycological,
        transactionType
      ];
}

List<Transaction> dummyTransaction = [
  Transaction(
      id: 1,
      quantity: 1,
      shippingCost: 0,
      total: 150000,
      status: "proses",
      orderNumber: "",
      deliveryService: "",
      invoice: "INV/20210110/XX/VI/566817981",
      receipt: "",
      orderDate: "21 Jan 2021",
      payment: "BCA Virtual Account",
      product: Product(),
      school: School(
        name: "Kinderland Preschool",
        price: 150000,
        picturePath: "assets/kindergarten_1.jpg",
      ),
      loan: Loan(),
      elearning: Elearning(),
      consultant: Consultant(),
      psycological: Psycological(),
      transactionType: TransactionType.school),
  Transaction(
      id: 2,
      quantity: 1,
      shippingCost: 0,
      total: 500000,
      status: "proses",
      orderNumber: "",
      deliveryService: "",
      invoice: "INV/20210110/XX/VI/566817981",
      receipt: "",
      orderDate: "10 Jan 2021",
      payment: "BCA Virtual Account",
      product: Product(),
      school: School(),
      loan: Loan(
        name: "Cashwagon",
        total: 500000,
        picturePath: "assets/cashwagon.png",
      ),
      elearning: Elearning(),
      consultant: Consultant(),
      psycological: Psycological(),
      transactionType: TransactionType.loan),
  Transaction(
      id: 3,
      quantity: 1,
      shippingCost: 0,
      total: 240000,
      status: "selesai",
      orderNumber: "",
      deliveryService: "Grab-Instant Courier",
      invoice: "INV/20210110/XX/VI/566817181",
      receipt: "GB-11-309830219",
      orderDate: "10 Jan 2021",
      payment: "BCA Virtual Account",
      product: Product(
        name: "Sepatu Baru",
        price: 240000,
        picturePath: "assets/shoes_1.jpg",
        seller: Seller(name: "Toko Sepatu Serba Ada"),
      ),
      school: School(),
      loan: Loan(),
      elearning: Elearning(),
      consultant: Consultant(),
      psycological: Psycological(),
      transactionType: TransactionType.product),
  Transaction(
      id: 4,
      quantity: 1,
      shippingCost: 0,
      total: 125000,
      status: "selesai",
      orderNumber: "",
      deliveryService: "",
      invoice: "INV/20210110/XX/VI/566817181",
      receipt: "",
      orderDate: "10 Jan 2021",
      payment: "BCA Virtual Account",
      product: Product(),
      school: School(),
      loan: Loan(),
      elearning: Elearning(
          lecturer: "Ruang Guru", picturePath: "assets/ruang_guru.jpg"),
      consultant: Consultant(),
      psycological: Psycological(),
      transactionType: TransactionType.elearning),
  Transaction(
      id: 5,
      quantity: 1,
      shippingCost: 0,
      total: 200000,
      status: "selesai",
      orderNumber: "",
      deliveryService: "",
      invoice: "INV/20210110/XX/VI/566817181",
      receipt: "",
      orderDate: "10 Jan 2021",
      payment: "BCA Virtual Account",
      product: Product(),
      school: School(),
      loan: Loan(),
      elearning: Elearning(),
      consultant: Consultant(
          name: "C. Vile Wright, PhD", picturePath: "assets/user_pic.jpg"),
      psycological: Psycological(),
      transactionType: TransactionType.consultation),
  Transaction(
      id: 6,
      quantity: 1,
      shippingCost: 0,
      total: 100000,
      status: "selesai",
      orderNumber: "",
      deliveryService: "",
      invoice: "INV/20210110/XX/VI/566817181",
      receipt: "",
      orderDate: "10 Jan 2021",
      payment: "BCA Virtual Account",
      product: Product(),
      school: School(),
      loan: Loan(),
      elearning: Elearning(),
      consultant: Consultant(),
      psycological: Psycological(
          name: "IQ Test", picturePath: "assets/psycological_1.jpg"),
      transactionType: TransactionType.psycological),
];
