part of 'models.dart';

class Psycological extends Equatable {
  final int id;
  final String name;
  final String providerName;
  final double rate;
  final int price;
  final String psycologicalDate;
  final String picturePath;

  Psycological({
    this.id,
    this.name,
    this.providerName,
    this.rate,
    this.price,
    this.psycologicalDate,
    this.picturePath,
  });

  @override
  List<Object> get props => [id, providerName, rate, price, picturePath];
}

List<Psycological> dummyPsycological = [
  Psycological(
      id: 1,
      name: "IQ Test",
      providerName: "Hallo Doc",
      rate: 4,
      price: 100000,
      psycologicalDate: "10-10-2020",
      picturePath: 'assets/psycological_1.jpg'),
  Psycological(
      id: 2,
      name: "IQ",
      providerName: "Premier",
      rate: 4,
      price: 200000,
      psycologicalDate: "11-10-2020",
      picturePath: 'assets/psycological_2.png'),
  Psycological(
      id: 3,
      name: "EQ Test",
      providerName: "Therapy Klinik",
      rate: 4,
      price: 200000,
      psycologicalDate: "12-10-2020",
      picturePath: 'assets/psycological_3.jpg'),
  Psycological(
      id: 4,
      name: "IQ Test",
      providerName: "Therapy Klinik",
      rate: 4,
      price: 200000,
      psycologicalDate: "13-10-2020",
      picturePath: 'assets/psycological_4.png')
];
